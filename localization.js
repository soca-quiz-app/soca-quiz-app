const translation = {
    idn: {
        fullName: "Nama Lengkap",
        start: "Mulai",
        settings: "Pengaturan",
        selectSpaces: "Pilih Space",
        selectMusics: "Pilih Music",
        language: 'Bahasa Indonesia',
        team: 'Tim',
        participant: 'Peserta',
        waitingText: 'Menunggu guru untuk memulai ...',
        watingPlayer: 'Menunggu Peserta ...'
    },
    en: {
        fullName: "Full Name",
        start: "Start",
        settings: "Settings",
        selectSpaces: "Select Spaces",
        selectMusics: "Select Musics",
        language: 'English',
        team: 'Team',
        participant: 'Participant',
        waitingText: 'Waiting for the quiz to start ...',
        watingPlayer: 'Waiting Participant ...'
    }
}
export default translation