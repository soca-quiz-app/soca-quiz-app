import { StyleSheet } from 'react-native';

const textStyles = StyleSheet.create({
  normalText: {
    fontFamily: 'avenir-next-reguler',
    color: '#fff',
  },
  semiBoldText: {
    fontFamily: 'avenir-next-demibold',
    color: '#fff',
    fontWeight: '500',
  },
  boldText: {
    fontFamily: 'avenir-next-bold',
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default textStyles;
