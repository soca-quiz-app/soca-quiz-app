import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeSVG from '../../assets/image/icon/Home.svg';
import ScanSVG from '../../assets/image/icon/Scan.svg';
import ProfileSVG from '../../assets/image/icon/Profile.svg';
import HomeUseSVG from '../../assets/image/icon/HomeFocused.svg';
import ScanUseSVG from '../../assets/image/icon/ScanFocused.svg';
import ProfileUseSVG from '../../assets/image/icon/ProfileUse.svg';
import HomeScreen from '../screens/HomeScreen';
import JoinScreen from '../screens/JoinScreen';
import Profile from '../screens/Profile';

const Tab = createBottomTabNavigator();

const BottomNavigation = () => {
  return (
    <Tab.Navigator
      style={{ backgroundColor: 'black' }}
      initialRouteName="Home"
      screenOptions={({ route }) => ({
        tabBarItemStyle: {
          height: 53,
        },
        tabBarStyle: {
          height: 53,
        },
        tabBarIcon: ({ focused, color, size }) => {
          let rn = route.name;

          if (rn === 'Home') {
            return focused ? <HomeUseSVG /> : <HomeSVG />;
          } else if (rn === 'Scan') {
            return focused ? <ScanUseSVG /> : <ScanSVG />;
          } else if (rn === 'Profile') {
            return focused ? <ProfileUseSVG/> : <ProfileSVG />;
          }
        },
        tabBarShowLabel: false,
        tabBarActiveBackgroundColor: 'black',
        tabBarInactiveBackgroundColor: 'black',
        // tabBarInactiveTintColor: 'black',
      })}
    >
      <Tab.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
      <Tab.Screen name="Scan" component={JoinScreen} options={{ headerShown: false }} />
      <Tab.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
    </Tab.Navigator>
  );
};

export default BottomNavigation;

const styles = StyleSheet.create({});
