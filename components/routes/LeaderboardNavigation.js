import { createStackNavigator } from "@react-navigation/stack";
import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import BottomNavigation from "./BottomNavigation";
import HomePlay from "../screens/HomePlay";
import LobbyRamean from "../screens/LobbyRamean";
import LeaderboardScreen from "../screens/LeaderboardScreen";

const Stack = createStackNavigator()

const LeaderboardNavigation = () => {
  return (
    <Stack.Navigator>
        <Stack.Screen name="LeaderboardScreen" component={LeaderboardScreen} options={{ headerShown: false}}/>
    </Stack.Navigator>
  )
}

export default LeaderboardNavigation
