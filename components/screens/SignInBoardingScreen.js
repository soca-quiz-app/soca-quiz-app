import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity } from 'react-native';
import React, { useState, useEffect } from 'react';
import BigLogo from '../../assets/image/boarding/BigLogo.svg';
import NextArrow from '../../assets/image/arrow/NextArrow.svg';
import Animated, { SlideInRight, SlideOutRight } from 'react-native-reanimated';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SignInBoardingView from '../views/SignInBoardingView';

const SignInBoardingScreen = ({ navigation }) => {
  const [visited, setVisited] = useState(false);

  useEffect(() => {
    AsyncStorage.getItem('screenVisited').then((value) => {
      if (value === null) {
        AsyncStorage.setItem('screenVisited', 'true');
        setVisited(false);
      } else {
        setVisited(true);
      }
    });
  }, []);
  return <SafeAreaView style={styles.container}>{visited ? navigation.replace('playNavigation') : <SignInBoardingView />}</SafeAreaView>;
};

export default SignInBoardingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: 'black',
  },
  welcomeContainer: {
    flex: 1,
    marginLeft: 28,
  },
  welcomeTitleText: {
    fontFamily: 'avenir-next-demibold',
    color: 'white',
    fontSize: 22,
  },
  welcomeDescContainer: {
    marginTop: 8,
  },
  welcomeDescText: {
    color: 'white',
    fontSize: 16,
    fontFamily: 'avenir-next-reguler',
  },
  welcomeDescText2: {
    color: 'white',
    marginTop: 10,
    fontSize: 16,
    fontFamily: 'avenir-next-reguler',
  },
  toHomeContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  toHomeButtonStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  toHomeText: {
    fontFamily: 'avenir-next-demibold',
    fontSize: 20,
    color: '#6D75F6',
  },
});
