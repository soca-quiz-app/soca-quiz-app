import { StyleSheet, Text, View, ImageBackground, Dimensions, TouchableOpacity, Image } from 'react-native';
import React, { useState } from 'react';
import CloseSVG from '../../assets/image/icon/Close.svg';
import SoundSVG from '../../assets/image/icon/VolumeUp.svg';
import { SafeAreaView } from 'react-native-safe-area-context';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import UserSVG from '../../assets/image/icon/User.svg';
import CalenderSVG from '../../assets/image/icon/Calendar.svg';
import PlaySVG from '../../assets/image/icon/Play.svg';
import QuestionSVG from '../../assets/image/icon/Question.svg';
import StarLikedSVG from '../../assets/image/icon/StarLiked.svg';
import StarUnlikedSVG from '../../assets/image/icon/StarUnliked.svg';
import LeaderboardPlayerCardView from '../views/LeaderboardPlayerCardView';
import ButonView from '../views/ButonView';
import ButtonViewCustom from '../views/ButtonViewCustom';

const LeaderboardScreen = ({ navigation }) => {
  const [dataPlayer, setDataPlayer] = useState([
    {
      id: 1,
      playername: 'Zhofron Al Fajr Gunarko Putra AAAAAAAAAAAAAAAAAAAAAAAAaa',
      poin: 90,
      correctanswer: 99,
      wronganswer: 1,
      profilepic: require('../../assets/image/avatar/Male-SoftwareEngineer.png'),
      borderstatus: false,
    },
    {
      id: 2,
      playername: 'Kevin',
      poin: 80,
      correctanswer: 8,
      wronganswer: 2,
      profilepic: require('../../assets/image/avatar/Male-Doctor.png'),
      borderstatus: false,
    },
    {
      id: 3,
      playername: 'Yuna',
      poin: 80,
      correctanswer: 8,
      wronganswer: 2,
      profilepic: require('../../assets/image/avatar/Male-Scientist.png'),
      borderstatus: true,
    },
    {
      id: 4,
      playername: 'Fina',
      poin: 70,
      correctanswer: 7,
      wronganswer: 3,
      profilepic: require('../../assets/image/avatar/Male-SoftwareEngineer.png'),
      borderstatus: false,
    },
    {
      id: 5,
      playername: 'Flora',
      poin: 60,
      correctanswer: 6,
      wronganswer: 4,
      profilepic: require('../../assets/image/avatar/Male-Scientist.png'),
      borderstatus: false,
    },
    {
      id: 6,
      playername: 'Shuri',
      poin: 60,
      correctanswer: 6,
      wronganswer: 4,
      profilepic: require('../../assets/image/avatar/Male-Doctor.png'),
      borderstatus: false,
    },
  ]);

  const [dataQuiz, setDataQuiz] = useState([
    {
      id: 1,
      title: 'Sistem Pemasaran Dasar',
      playTime: 320,
      question: 10,
      like: 200,
      image: require('../../assets/image/logo/PemasaranPenjualan.png'),
      liked: false,
    },
    {
      id: 2,
      title: 'Simple Present Dasar',
      playTime: 320,
      question: 10,
      like: 200,
      image: require('../../assets/image/logo/BahasaInggris.png'),
      liked: true,
    },
    {
      id: 3,
      title: 'Bahasa Indonesia Dasar',
      playTime: 320,
      question: 10,
      like: 200,
      image: require('../../assets/image/logo/BahasaIndonesia.png'),
      liked: false,
    },
    {
      id: 4,
      title: 'Sistem Pemasaran Dasar',
      playTime: 320,
      question: 10,
      like: 200,
      image: require('../../assets/image/logo/PemasaranPenjualan.png'),
      liked: false,
    },
  ]);

  const backToPlay = () => {
    // navigation.navigate('HomeNav');
    console.log('Pressed');
  };

  return (
    <ImageBackground source={require('../../assets/image/background/Background.png')} style={styles.containerbackground}>
      <View style={styles.containerleaderboard}>
        <Text style={styles.bigCardTitleText}>Leaderboard</Text>
        <SafeAreaView style={styles.scrolContainer}>
          <View style={styles.headerContainer}>
            <View style={styles.headerContainerLeft}>
              <Text style={styles.HomeText}>Rank</Text>
              <Text style={styles.HomeText}>Peserta</Text>
            </View>
            <View style={styles.headerContainerRight}>
              <Text style={styles.HomeText}>Rank Poin</Text>
            </View>
          </View>

          {/* List Player */}
          <FlatList
            horizontal={false}
            data={dataPlayer}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => (
              <LeaderboardPlayerCardView
                rank={item.id}
                playername={item.playername}
                poin={item.poin}
                correctanswer={item.correctanswer}
                wronganswer={item.wronganswer}
                profilepic_uri={item.profilepic}
                borderstatus={item.borderstatus}
              ></LeaderboardPlayerCardView>
            )}
          />
        </SafeAreaView>
        <ButtonViewCustom text="Main Lagi?" onPress={backToPlay} backgroundColor="#40BE45" marginBottom={20} />
      </View>

      <View style={styles.containerextraquiz}>
        {/* Daily Quiz */}
        <Text
          style={{
            fontFamily: 'avenir-next-demibold',
            color: 'white',
            marginLeft: 30,
            marginTop: 10,
          }}
        >
          Quiz Sejenis
        </Text>

        {/* List Quiz */}
        <FlatList
          style={{ flexGrow: 0, marginTop: 7, marginLeft: 24 }}
          horizontal={true}
          data={dataQuiz}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <View style={styles.quizContainer}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('HomePlay', item.id);
                }}
              >
                <Image source={item.image} />
              </TouchableOpacity>
              <Text style={styles.quizTitleText}>{item.title}</Text>
              <View style={styles.quizFooterContainer}>
                <View style={styles.quizFooter}>
                  <PlaySVG />
                  <Text style={styles.quizText}>{item.playTime}</Text>
                </View>
                <View style={styles.quizFooter}>
                  <QuestionSVG />
                  <Text style={styles.quizText}>{item.question}</Text>
                </View>
                <View style={styles.quizFooter}>
                  {item.liked ? <StarLikedSVG /> : <StarUnlikedSVG />}
                  <Text style={styles.quizText}>{item.playTime}</Text>
                </View>
              </View>
            </View>
          )}
        />
        <ButtonViewCustom text="Temukan Quiz Lainnya >" onPress={backToPlay} backgroundColor="#40BE4500" width={200} height={20} alignSelf="center" marginBottom={10} color="#FDBA59" />
      </View>
    </ImageBackground>
  );
};

export default LeaderboardScreen;

const styles = StyleSheet.create({
  containerbackground: {
    height: '100%',
  },
  scrolContainer: {
    //   flex: 1,
    //   backgroundColor: 'black',
    maxHeight: '82%',
  },
  containerleaderboard: {
    //   flex: 1,
    //   zIndex:-1,
    backgroundColor: '#00000099',
    //   maxHeight:200,
    // height:200,
    // opacity:0.5,
    marginHorizontal: 10,
    borderRadius: 20,
    marginTop: 80,
    maxHeight: '60%',
  },
  containerextraquiz: {
    // flex: 1,
    height: 230,
    marginVertical: 20,
    // maxHeight: 200,
    backgroundColor: '#00000099',
    marginHorizontal: 10,
    borderRadius: 20,
    // opacity:0.2,
    marginBottom: 30,
  },
  headerContainer: {
    // flex:1,
    flexDirection: 'row',
    //   alignItems: 'center',
    //   justifyContent: "flex-start",
    marginLeft: 10,
    marginRight: 20,
    paddingHorizontal: 15,
    //   width:"100%",
    //   backgroundColor:'blue',
    //   alignContent: "flex-end",
    //   marginBottom: 24,
  },
  headerContainerRight: {
    flex: 1,
    flexDirection: 'row-reverse',
    //   alignContent: "flex-end",
    //   alignSelf: "flex-end",
    //   backgroundColor:'red',
    //   width:'50%',
    //   marginRight: 20,
  },
  headerContainerLeft: {
    flex: 1,
    flexDirection: 'row',
    // justifyContent: "space-between",
    // width:'50%',
    // backgroundColor:'green',
    // marginRight: 20,
  },
  searcBarContainer: {
    flexDirection: 'row',
    backgroundColor: '#222222',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    marginTop: 23,
    marginLeft: 25,
    marginRight: 24,
    borderRadius: 4,
  },
  searchTextInputStyle: {
    marginLeft: 11,
    color: 'white',
    fontFamily: 'avenir-next-demibold',
    width: '90%',
    fontSize: 12,
  },
  bigCardContainer: {
    flexDirection: 'row',
    backgroundColor: '#222222',
    alignContent: 'flex-end',
    justifyContent: 'flex-start',
    marginLeft: 24,
    marginTop: 26,
    marginRight: 25,
    borderRadius: 10,
  },
  bigCardRightContentContainer: {
    marginRight: 19,
    width: 365,
  },
  bigCardTitleText: {
    fontFamily: 'avenir-next-medium',
    color: 'white',
    //   width: 200,
    marginTop: 15,
    marginLeft: 20,
    fontWeight: 'bold',
    marginBottom: -25,
    fontSize: 16,
    //   marginRight: 33,
    //   marginTop: 27,
  },
  bigCardDescText: {
    color: '#9D9D9D',
    width: 172,
    fontFamily: 'avenir-next-reguler',
    fontSize: 12,
    marginTop: 7,
  },
  learnButtonStyle: {
    backgroundColor: '#BC3BF2',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    borderRadius: 22,
    marginRight: 19,
    marginTop: 14,
    marginBottom: 15,
  },
  learnButtonText: {
    color: 'white',
    paddingVertical: 5,
    paddingRight: 10,
    paddingLeft: 12,
    fontSize: 10,
    fontFamily: 'avenir-next-demibold',
  },
  HomeText: {
    // flex:1,
    fontSize: 14,
    fontFamily: 'avenir-next-reguler',
    color: 'white',
    marginTop: 10,
    marginLeft: 10,
  },
  cardAtHomeContainer: {
    backgroundColor: '#222222',
    flexDirection: 'row',
    marginLeft: 8,
    borderRadius: 10,
  },
  cardAtHomeLeftSideContainer: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 15,
    marginLeft: 17,
  },
  atHomeLeftSideText: {
    fontSize: 10,
    fontFamily: 'avenir-next-reguler',
    color: 'white',
    marginTop: 7,
    marginBottom: 15,
    width: 123,
  },
  cardAtHomeRightSideContainer: {
    marginRight: 12,
    marginTop: 14,
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  cardAthHomeUserContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  userText: {
    color: 'white',
    fontFamily: 'avenir-next-reguler',
    fontSize: 8,
  },
  joinButtonStyle: {
    backgroundColor: '#BC3BF2',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    marginBottom: 11,
    borderRadius: 22,
  },
  joinButtonText: {
    fontFamily: 'avenir-next-medium',
    fontSize: 8,
    color: 'white',
    paddingHorizontal: 12,
    paddingVertical: 3,
  },
  quizContainer: {
    marginLeft: 6,
    borderRadius: 5,
    backgroundColor: '#222222',
  },
  quizTitleText: {
    fontFamily: 'avenir-next-demibold',
    color: 'white',
    fontSize: 9,
    marginLeft: 7,
    marginTop: 6,
    width: 76,
  },
  quizFooterContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  quizFooter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 6,
    marginBottom: 9,
  },
  quizText: {
    color: '#C1C1C1',
    fontSize: 6,
    fontFamily: 'avenir-next-reguler',
    marginLeft: 2,
  },
  adventureContainer: {
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 9,
    backgroundColor: '#222222',
    borderRadius: 10,
    marginBottom: 84,
  },
  adventureLeftContainer: {
    marginTop: 26,
    marginLeft: 23,
    marginBottom: 17,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  adventureTitleText: {
    fontFamily: 'avenir-next-reguler',
    fontSize: 14,
    color: 'white',
    width: 181,
  },
  adventureFooterContainer: {
    marginTop: 20,
    flexDirection: 'row',
    alignItem: 'center',
    justifyContent: 'flex-start',
  },
  adventureFooter: {
    flexDirection: 'row',
    alignItem: 'center',
    justifyContent: 'center',
  },
  adventureText: {
    color: '#C1C1C1',
    fontSize: 10,
    fontFamily: 'avenir-next-reguler',
    marginLeft: 3,
    marginRight: 11,
  },
});
