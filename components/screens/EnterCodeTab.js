import { StyleSheet, Text, View, TextInput, TouchableHighlight } from 'react-native';
import React, { useState } from 'react';
import textStyles from '../styles/TextStyles';
import HorizontalLine from '../../assets/HorizontalLine.svg';
import RightArrow from '../../assets/RightArrow.svg';

const EnterCodeTab = () => {
  const [lengthCount, setLengthCount] = useState(0);
  return (
    <View style={styles.container}>
      <Text style={[textStyles.semiBoldText, { fontSize: 18, marginTop: 33, flex: 1 }]}>Join and have fun together!</Text>
      <View style={{ flex: 2, alignItems: 'center' }}>
        <Text style={[textStyles.semiBoldText, { fontSize: 20, marginBottom: 79 }]}>Enter PIN</Text>
        <TextInput style={styles.formStyle} placeholder="" placeholderTextColor="#8C8C8C" keyboardType="number-pad" maxLength={6} onChangeText={setLengthCount} />
        <HorizontalLine />
        {lengthCount.length == 6 ? (
          <TouchableHighlight style={styles.buttonContainer} onPress={null}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={[textStyles.semiBoldText, { fontSize: 16, marginEnd: 9 }]}>Join</Text>
              <RightArrow />
            </View>
          </TouchableHighlight>
        ) : null}
      </View>
    </View>
  );
};

export default EnterCodeTab;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
  },
  formStyle: {
    fontFamily: 'avenir-next-demibold',
    fontSize: 40,
    color: '#fff',
    padding: 10,
    width: 180,
    marginBottom: 10,
  },
  buttonContainer: {
    backgroundColor: '#6D75F6',
    paddingVertical: 6,
    paddingHorizontal: 27,
    borderRadius: 4,
    marginTop: 38,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
