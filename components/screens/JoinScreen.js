import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableHighlight, useWindowDimensions } from 'react-native';
import React, { useState } from 'react';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import ScanQrTab from './ScanQrTab';
import EnterCodeTab from './EnterCodeTab';
import TopBarView from '../views/TopBarView';
import LeftArrow from '../../assets/LeftArrow.svg';
import TextInputLayout from '../views/TextInputLayout';
import ButonView from '../views/ButonView';
import textStyles from '../styles/TextStyles';

const renderScene = SceneMap({
  qr: ScanQrTab,
  code: EnterCodeTab,
});

const JoinScreen = ({ navigation }) => {
  const layout = useWindowDimensions();

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: 'qr', title: 'Scan QR' },
    { key: 'code', title: 'Enter PIN' },
  ]);

  const renderTabBar = (props) => {
    return (
      <TabBar
        {...props}
        style={{ backgroundColor: '#000' }}
        renderLabel={({ route, focused }) => <Text style={[focused ? textStyles.semiBoldText : textStyles.normalText, { color: focused ? '#BC3BF2' : '#fff', margin: 6, fontSize: 16 }]}>{route.title}</Text>}
        indicatorStyle={{ backgroundColor: '#BC3BF2' }}
      />
    );
  };
  return (
    <View style={styles.mainContainer}>
      <View style={styles.header}>
        <TopBarView />
      </View>
      <View style={styles.container}>
        <TabView navigationState={{ index, routes }} renderScene={renderScene} onIndexChange={setIndex} initialLayout={{ width: layout.width }} renderTabBar={renderTabBar} />
      </View>
      {/* <View style={styles.footer}>
        <Text style={styles.footerText}>Copyright © 2021 Soca.ai All Reserved</Text>
      </View> */}
    </View>
  );
};

export default JoinScreen;

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'column',
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: '#000',
    marginTop: StatusBar.currentHeight || 0,
  },
  header: {
    backgroundColor: '#000',
  },
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: '#000',
  },
  // footer: {
  //   backgroundColor: '#000',
  //   marginBottom: 26,
  // },
  back: {
    marginTop: 60,
    marginStart: 28,
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerText: {
    fontFamily: 'avenir-next-reguler',
    color: '#A6A6A6',
    fontSize: 12,
    textAlign: 'center',
  },
});
