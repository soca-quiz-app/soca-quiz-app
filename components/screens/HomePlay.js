import { StyleSheet, Text, View, ImageBackground, TouchableOpacity, TextInput, Dimensions, Switch, FlatList, Image, Alert } from 'react-native';
import React, { useState, useEffect, useRef } from 'react';
import CloseSVG from '../../assets/image/icon/Close.svg';
import SoundSVG from '../../assets/image/icon/VolumeUp.svg';
import PlayModal from '../views/PlayModal';
import Animated, { SlideInRight, SlideOutRight } from 'react-native-reanimated';
import { Audio } from 'expo-av';
import translations from '../../localization';
// import * as Localization from 'expo-localization';
import { I18n } from 'i18n-js';

const HomePlay = ({ navigation, route }) => {
  //set bahasa
  const i18n = new I18n(translations);
  i18n.locale = 'eng';
  i18n.enableFallback = true;

  //bahasa
  const [locale, setLocale] = useState(i18n.locale);
  const changeLocale = (locale) => {
    i18n.locale = locale;
    setLocale(locale);
  };

  //data
  const [quizId, setQuizId] = useState(route.id);
  const [username, setUsername] = useState(null);
  const [isEnabled, setIsEnabled] = useState(false);
  const [indexSpace, setIndexSpace] = useState(null);
  const [indexMusic, setIndexMusic] = useState(false);
  const [background, setBackground] = useState(require('../../assets/image/background/Background.png'));

  // modal status
  const [modalVisibleStatus, setModalVisibleStatus] = useState(true);

  const [dataSpace, setDataSpace] = useState([
    {
      id: 1,
      name: 'Astronaut',
      image: require('../../assets/image/background/Bg-Space.png'),
      image_bg: require('../../assets/image/background/Background.png'),
    },
    {
      id: 2,
      name: 'Camping',
      image: require('../../assets/image/background/Bg-Camp.png'),
      image_bg: require('../../assets/image/background/Camp-bg.png'),
    },
    {
      id: 3,
      name: 'Cafe',
      image: require('../../assets/image/background/Bg-Cafe.png'),
      image_bg: require('../../assets/image/background/Background.png'),
    },
    {
      id: 4,
      name: 'Beach',
      image: require('../../assets/image/background/Bg-Beach.png'),
      image_bg: require('../../assets/image/background/Camp-bg.png'),
    },
    {
      id: 5,
      name: 'Waterfall',
      image: require('../../assets/image/background/Bg-Beach.png'),
      image_bg: require('../../assets/image/background/Background.png'),
    },
  ]);

  const [dataMusic, setDataMusic] = useState([
    {
      id: 1,
      name: 'Metal',
      image: require('../../assets/image/background/Metal.png'),
      music_path: require('../../assets/sound/Visions.mp3'),
    },
    {
      id: 2,
      name: 'Classic',
      image: require('../../assets/image/background/Classical.png'),
      music_path: require('../../assets/sound/BadHabit.mp3'),
    },
    {
      id: 3,
      name: 'Rock',
      image: require('../../assets/image/background/Rock.png'),
      music_path: require('../../assets/sound/Visions.mp3'),
    },
    {
      id: 4,
      name: 'Beat',
      image: require('../../assets/image/background/Beat.png'),
      music_path: require('../../assets/sound/BadHabit.mp3'),
    },
    {
      id: 5,
      name: 'Techno',
      image: require('../../assets/image/background/Beat.png'),
      music_path: require('../../assets/sound/Visions.mp3'),
    },
  ]);

  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  const goToLobbyHandler = () => {
    if (username === null || username === '') {
      Alert.alert('Username Tidak boleh kosong');
    } else {
      // navigation.replace("LobbySeruan", {username: username, background: background})
      navigation.replace('LobbyRamean', {
        username: username,
        background: background,
      });
      // navigation.replace("LobbyDuel", {username: username, background: background})
    }
  };

  //play sound
  const [sound, setSound] = useState(null);
  const [soundStatus, setSoundStatus] = useState(false);

  async function playSound(path, musicId) {
    if (sound !== null) {
      unloadSound();
    }
    console.log('Loading Sound');
    const { sound } = await Audio.Sound.createAsync(path);
    setSound(sound);
    setIndexMusic(musicId);
    setSoundStatus(true);

    console.log('Playing Sound');
    await sound.playAsync();
  }

  async function pauseSound() {
    console.log('Pausing Sound');
    await sound.pauseAsync();
    setSoundStatus(false);
  }

  async function resumeSound() {
    console.log('Resume Sound');
    await sound.playAsync();
    setSoundStatus(true);
  }

  async function unloadSound() {
    console.log('Unloading Sound');
    await sound.stopAsync();
    await sound.unloadAsync();
    setSoundStatus(false);
    setSound(null);
    setIndexMusic(null);
  }

  return (
    <ImageBackground source={background} style={styles.container}>
      {/* Header */}
      <View style={styles.headerContainer}>
        {/* Close Icon */}
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <CloseSVG />
        </TouchableOpacity>

        {/* Volume Icon */}
        <TouchableOpacity
          onPress={() => {
            soundStatus ? pauseSound() : resumeSound();
          }}
        >
          <SoundSVG />
        </TouchableOpacity>
      </View>

      {/* Input Name */}
      <View style={styles.nameContainer}>
        <Text style={styles.nameTitleText}>{i18n.t('fullName')}</Text>
        <TextInput placeholder="Heri Tipsi" style={styles.nameTextInputStyle} onChangeText={(val) => setUsername(val)} value={username} />
        <TouchableOpacity style={styles.submitNameButtonStyle} onPress={() => goToLobbyHandler()}>
          <Text style={styles.submitNameText}>{i18n.t('start')}</Text>
        </TouchableOpacity>
      </View>

      {/* Settings */}
      <Text style={styles.titleText}>{i18n.t('settings')}</Text>

      <View style={styles.settingsContainer}>
        <Text style={styles.settingsText}>Audio</Text>
        <Switch trackColor={{ false: 'gray', true: '#40BE45' }} thumbColor={'white'} ios_backgroundColor="grey" onValueChange={toggleSwitch} value={isEnabled} />
      </View>

      {/* Space */}
      <View style={styles.spaceContainer}>
        {/* Choose Space */}
        <Text style={styles.settingsText}>{i18n.t('selectSpaces')}</Text>

        {/* List Space */}
        <FlatList
          style={{ flexGrow: 0, marginTop: 10 }}
          horizontal={true}
          data={dataSpace}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <Animated.View style={styles.cardSpaceContainer} entering={SlideInRight.duration(1000)} exiting={SlideOutRight.duration(1000)}>
              <TouchableOpacity
                onPress={() => {
                  setIndexSpace(item.id);
                  setBackground(item.image_bg);
                }}
              >
                <Image
                  source={item.image}
                  style={{
                    borderWidth: indexSpace === item.id ? 1.5 : 0,
                    borderColor: '#40BE45',
                  }}
                />
                <Text style={styles.spaceText}>{item.name}</Text>
              </TouchableOpacity>
            </Animated.View>
          )}
        />

        {/* Music */}
        <Text style={styles.musicTitleText}>{i18n.t('selectMusics')}</Text>

        {/* List Music */}
        <FlatList
          style={{ flexGrow: 0, marginTop: 11 }}
          horizontal={true}
          data={dataMusic}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <Animated.View style={styles.cardSpaceContainer} entering={SlideInRight.duration(1000)} exiting={SlideOutRight.duration(1000)}>
              <TouchableOpacity
                onPress={() => {
                  playSound(item.music_path, item.id);
                }}
              >
                <Image
                  source={item.image}
                  style={{
                    borderWidth: indexMusic === item.id ? 1.5 : 0,
                    borderColor: '#40BE45',
                  }}
                />
                <Text style={styles.spaceText}>{item.name}</Text>
              </TouchableOpacity>
            </Animated.View>
          )}
        />
      </View>
      <PlayModal modalVisible={modalVisibleStatus} handleOnClose={() => setModalVisibleStatus(false)} handleOnSkip={() => setModalVisibleStatus(false)} />
    </ImageBackground>
  );
};

export default HomePlay;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    flex: 1,
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height + 50,
  },
  headerContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 31,
    marginRight: 31,
    marginTop: 45,
  },
  nameContainer: {
    backgroundColor: '#1B1B1B',
    marginHorizontal: 31,
    marginTop: 37,
    paddingHorizontal: 27,
    paddingVertical: 16,
  },
  nameTitleText: {
    fontFamily: 'avenir-next-reguler',
    fontSize: 12,
    color: 'white',
  },
  nameTextInputStyle: {
    marginTop: 7,
    backgroundColor: 'white',
    padding: 8,
    borderRadius: 2,
    marginBottom: 11,
  },
  submitNameButtonStyle: {
    backgroundColor: '#6D75F6',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    paddingVertical: 8,
  },
  submitNameText: {
    color: 'white',
    fontFamily: 'avenir-next-medium',
    fontSize: 14,
  },
  titleText: {
    fontFamily: 'avenir-next-demibold',
    fontSize: 14,
    color: 'white',
    marginLeft: 31,
    marginTop: 18,
  },
  settingsContainer: {
    backgroundColor: '#1B1B1B',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 27,
    paddingVertical: 16,
    marginHorizontal: 31,
    borderRadius: 8,
  },
  settingsText: {
    fontFamily: 'avenir-next-reguler',
    fontSize: 12,
    color: 'white',
  },
  spaceContainer: {
    marginTop: 18,
    backgroundColor: '#1B1B1B',
    paddingLeft: 31,
    paddingTop: 18,
    paddingBottom: 20,
  },
  cardSpaceContainer: {
    marginLeft: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  spaceText: {
    fontFamily: 'avenir-next-reguler',
    color: 'white',
    fontSize: 10,
    alignSelf: 'center',
    marginTop: 7,
  },
  musicTitleText: {
    fontFamily: 'avenir-next-demibold',
    fontSize: 12,
    color: 'white',
    marginTop: 15,
  },
});
