import { StyleSheet, Text, View, ImageBackground, Dimensions, TouchableOpacity, Image } from 'react-native';
import React, { useState } from 'react';
import CloseSVG from '../../assets/image/icon/Close.svg';
import SoundSVG from '../../assets/image/icon/VolumeUp.svg';
import SettingSVG from '../../assets/image/icon/Setting.svg';
import CountSVG from '../../assets/image/icon/CountUser.svg';
import VersusSVG from '../../assets/image/icon/Versus.svg';
import { BlurView } from 'expo-blur';
import Animated, { SlideInDown, SlideOutDown } from 'react-native-reanimated';
import translations from '../../localization';
// import * as Localization from "expo-localization";
import { I18n } from 'i18n-js';

const LobbyDuel = ({ navigation, route }) => {
  //set bahasa
  const i18n = new I18n(translations);
  i18n.locale = 'eng';
  i18n.enableFallback = true;

  //bahasa
  const [locale, setLocale] = useState(i18n.locale);
  const changeLocale = (locale) => {
    i18n.locale = locale;
    setLocale(locale);
  };

  const [username, setUsername] = useState(route.params.username);
  const [background, setBackground] = useState(route.params.background);
  const [player1, setPlayer1] = useState({
    name: 'Asep',
    image: require('../../assets/image/avatar/DadangKarbit.png'),
  });

  const [player2, setPlayer2] = useState({
    name: 'Dadang',
    image: require('../../assets/image/avatar/DadangKarbit.png'),
  });

  const filterPlayerName = (name) => {
    let tempName = name;
    if (tempName.length > 7) {
      return tempName.substring(0, 6) + '...';
    }
    return tempName;
  };

  //timer
  const [timerCount, setTimer] = useState(5);

  useEffect(() => {
    let interval = setInterval(() => {
      setTimer((lastTimerCount) => {
        lastTimerCount <= 1 && clearInterval(interval);
        return lastTimerCount - 1;
      });
    }, 1000); //each count lasts for a second
    //cleanup the interval on complete
    return () => clearInterval(interval);
  }, []);

  return (
    <ImageBackground source={background} style={styles.container}>
      {timerCount == 0
        ? navigation.replace('LeaderboardScreen', {
            background: background,
          })
        : ''}
      {/* Header */}
      <View style={styles.headerContainer}>
        {/* Close Icon */}
        <TouchableOpacity style={styles.languageButtonStyle}>
          <Text style={styles.languageText}>{i18n.t('language')}</Text>
        </TouchableOpacity>

        {/* Right Icon */}
        <View style={styles.headerRightContainer}>
          <TouchableOpacity>
            <SoundSVG />
          </TouchableOpacity>
          <TouchableOpacity style={{ marginHorizontal: 6 }}>
            <SettingSVG />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.goBack()}>
            <CloseSVG />
          </TouchableOpacity>
        </View>
      </View>

      {/* <ScrollView style={styles.scrolContainer}> */}

      {/* Body Container */}
      <Animated.View style={styles.bodyContainer} entering={SlideInDown.duration(1000)} exiting={SlideOutDown.duration(1000)}>
        {/* User */}
        <BlurView intensity={80} tint="dark" style={styles.userContainer}>
          <Image source={require('../../assets/image/avatar/User.png')} style={{ marginLeft: 26, marginTop: 20, marginBottom: 20 }} />
          <View style={styles.userInsideContainer}>
            <Text style={styles.nameText}>{username}</Text>
            <Text style={styles.timText}>{locale === 'eng' ? `Serigala ${i18n.t('team')}` : `${i18n.t('team')} Serigala`}</Text>
          </View>
        </BlurView>

        {/* Player Container */}
        <BlurView intensity={80} tint="dark" style={styles.playerContainer}>
          <View style={styles.bodyHeaderContainer}>
            <CountSVG style={{ marginTop: 27 }} />
            <Text style={styles.countText}>2 {i18n.t('participant')}</Text>
          </View>

          {/* Waiting Text */}
          <Text style={styles.waitingText}>{i18n.t('waitingText')}</Text>

          {/* All player container */}
          <BlurView intensity={80} tint="dark" style={styles.versusContainer}>
            <View style={styles.singlePlayerContainer}>
              <Image source={player1.image} style={{ width: 36, height: 36 }} />
              <Text style={styles.playerNameText}>{player1.name}</Text>
            </View>
            <VersusSVG />
            <View style={styles.singlePlayerContainer}>
              <Image source={player2.image} style={{ width: 36, height: 36 }} />
              <Text style={styles.playerNameText}>{filterPlayerName(player2.name)}</Text>
            </View>
          </BlurView>
        </BlurView>
      </Animated.View>
    </ImageBackground>
  );
};

export default LobbyDuel;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    flex: 1,
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height + 50,
    paddingBottom: 105,
  },
  headerContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 14,
    marginRight: 19,
    marginTop: 45,
  },
  languageButtonStyle: {
    backgroundColor: 'white',
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderRadius: 17,
    alignItems: 'center',
    justifyContent: 'center',
  },
  languageText: {
    fontFamily: 'avenir-next-medium',
    fontSize: 12,
  },
  headerRightContainer: {
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
  },
  bodyContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 15,
    marginTop: 217,
    marginBottom: 271,
    height: 582,
  },
  userContainer: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginHorizontal: 15,
    marginTop: 47,
    borderRadius: 14,
    borderWidth: 0.88,
    alignSelf: 'stretch',
  },
  userInsideContainer: {
    marginLeft: 17,
    alignItems: 'flex-start',
    marginVertical: 25,
  },
  nameText: {
    color: 'white',
    fontSize: 18,
    fontFamily: 'avenir-next-medium',
  },
  timText: {
    color: 'white',
    fontSize: 12,
    fontFamily: 'avenir-next-reguler',
  },
  playerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 13,
    borderRadius: 14,
    borderWidth: 0.88,
    alignSelf: 'stretch',
    marginHorizontal: 15,
  },
  bodyHeaderContainer: {
    alignSelf: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 19,
    justifyContent: 'center',
  },
  countText: {
    marginTop: 31,
    marginLeft: 7,
    color: 'white',
    fontFamily: 'avenir-next-medium',
    fontSize: 12,
  },
  waitingText: {
    color: 'white',
    fontSize: 14,
    fontFamily: 'avenir-next-reguler',
    marginTop: 20,
    marginBottom: 5,
  },
  emptyUserText: {
    color: '#B7B7B7',
    fontSize: 10,
    fontFamily: 'avenir-next-reguler',
    marginTop: 28,
  },
  specificPlayerContainer: {
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderRadius: 8.44,
    borderWidth: 0.74,
    marginBottom: 13,
  },
  listNameText: {
    color: 'white',
    fontFamily: 'avenir-next-medium',
    width: 84,
    fontSize: 12,
    marginLeft: 10,
    marginRight: 19,
  },
  versusContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    marginHorizontal: 26,
    marginTop: 31,
    marginBottom: 33,
    borderWidth: 0.88,
    borderRadius: 10,
    paddingTop: 17,
    paddingBottom: 14,
    paddingHorizontal: 30,
  },
  singlePlayerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 66,
  },
  playerNameText: {
    color: 'white',
    fontFamily: 'avenir-next-medium',
    fontSize: 14,
  },
});
