import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Alert } from 'react-native';
import React, { useState, useEffect } from 'react';
import SocaLogo from '../../assets/SocaLogo.svg';
import TextInputLayout from '../views/TextInputLayout';
import ContinueAppleButton from '../views/ContinueAppleButton';
import ContinueGoogleButton from '../views/ContinueGoogleButton';
import ButonView from '../views/ButonView';
import Animated, { SlideInDown, SlideOutDown } from 'react-native-reanimated';
import axios from 'axios';
import * as WebBrowser from 'expo-web-browser';
import * as Google from 'expo-auth-session/providers/google';

const baseUrl = 'https://api-landingpage.soca.ai';

// web: 159695980112-ci1f9navcet8pk0ug6iornh9t5eokt1u.apps.googleusercontent.com
// iOS: 159695980112-3qc8qjgn7ls3114d95lpmj10i3ceeuul.apps.googleusercontent.com
// android: 159695980112-jmq8e2afb9p4okjiogdcu09v397htodl.apps.googleusercontent.com

WebBrowser.maybeCompleteAuthSession();

const SignInScreen = ({ navigation }) => {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [accessToken, setAccessToken] = useState(null);
  // const [userInfo, setUserInfo] = useState(null);
  const [request, response, promptAsync] = Google.useIdTokenAuthRequest({
    clientId: '159695980112-ci1f9navcet8pk0ug6iornh9t5eokt1u.apps.googleusercontent.com',
    iosClientId: '159695980112-3qc8qjgn7ls3114d95lpmj10i3ceeuul.apps.googleusercontent.com',
    androidClientId: '159695980112-jmq8e2afb9p4okjiogdcu09v397htodl.apps.googleusercontent.com',
  });

  useEffect(() => {
    if (response?.type === 'success') {
      setAccessToken(response.authentication.accessToken);
      accessToken && navigation.replace('signInBoardingScreen');
    }
  }, [response, accessToken]);

  // const getUserInfo = async () => {
  //   try {
  //     const response = await fetch('https://www.googleapis.com/userinfo/v2/me', {
  //       headers: { Authorization: `Bearer ${token}` },
  //     });

  //     const user = await response.json();
  //     setUserInfo(user);
  //   } catch (error) {
  //     // Add your own error handler here
  //   }
  // };

  const onLoginHandler = async (event) => {
    if (!email && !password) {
      Alert.alert('Tolong lengkapi formnya');
      return;
    } else if (!email) {
      Alert.alert('Isi email kamu');
      return;
    } else if (!password) {
      Alert.alert('Buat password kamu');
      return;
    } else {
      if (password.length < 8) {
        Alert.alert('Password minimal 8 karakter');
        return;
      } else {
        try {
          const response = await axios.post(`${baseUrl}/api/sign-in`, {
            email,
            password,
          });
          if (response.status === 200) {
            alert(`${JSON.stringify(response.data.message)}`);
            setEmail(null);
            setPassword(null);
            navigation.navigate('signInBoardingScreen');
          } else if (response.status === 400) {
            alert(`${JSON.stringify(response.data.error)}`);
          } else {
            throw new Error('An error has occurred');
          }
        } catch (error) {
          alert('An error has occurred');
        }
      }
    }
  };

  return (
    <ScrollView style={styles.scrolContainer}>
      <Animated.View style={styles.container} entering={SlideInDown.duration(500)} exiting={SlideOutDown.duration(500)}>
        <SocaLogo />
        <Text style={styles.title}>Login</Text>
        <ContinueAppleButton text="Continue with Apple" />
        <ContinueGoogleButton text="Continue with Google" borderRadius={10} onPress={() => promptAsync()} statate={!request} />
        <Text style={[styles.normalText, { marginTop: 20 }]}>atau masuk dengan</Text>
        <TextInputLayout label="Email" placeholder="Masukkan email kamu" keyboardType="email-address" onChangeText={(val) => setEmail(val)} value={email} />
        <TextInputLayout label="Password" placeholder="Buat password" secureTextEntry={true} onChangeText={(val) => setPassword(val)} value={password} />
        <View style={styles.forgotPasswordView}>
          <TouchableOpacity onPress={() => navigation.navigate('forgotPasswordScreen')}>
            <Text style={styles.textPressable}>Lupa password?</Text>
          </TouchableOpacity>
        </View>
        <ButonView text="Masuk" onPress={() => onLoginHandler()} />
        <View style={styles.toSignup}>
          <Text style={styles.normalText}>Belum punya akun?</Text>
          <TouchableOpacity style={{ marginStart: 4 }} onPress={() => navigation.navigate('signupScreen')}>
            <Text style={styles.textPressable}>Daftar disini</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.footer}>Copyright © 2023 Soca.ai All Reserved</Text>
      </Animated.View>
    </ScrollView>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  scrolContainer: {
    flex: 1,
    backgroundColor: '#000',
    marginTop: StatusBar.currentHeight || 0,
  },
  container: {
    alignItems: 'center',
    marginTop: 155,
  },
  title: {
    fontFamily: 'avenir-next-demibold',
    color: '#fff',
    fontWeight: '500',
    fontSize: 24,
    marginTop: 32,
  },
  normalText: {
    fontFamily: 'avenir-next-reguler',
    color: '#fff',
    fontSize: 12,
    textAlign: 'center',
  },
  textPressable: {
    fontFamily: 'avenir-next-demibold',
    color: '#6D75F6',
    fontSize: 12,
    fontWeight: '500',
    textAlign: 'center',
    alignSelf: 'center',
  },
  toSignup: {
    flexDirection: 'row',
    marginTop: 36,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    fontFamily: 'avenir-next-reguler',
    color: '#A6A6A6',
    fontSize: 12,
    textAlign: 'center',
    marginTop: 110,
  },
  forgotPasswordView: {
    marginEnd: 38,
    marginTop: 20,
    alignSelf: 'flex-end',
  },
});
