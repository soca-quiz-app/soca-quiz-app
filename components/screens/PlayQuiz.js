import { StyleSheet, Text, View, ImageBackground, Dimensions, TouchableOpacity, Image, Pressable, ActivityIndicator } from 'react-native';
import React, { useState, useEffect, useRef } from 'react';
import { Audio, Video, AVPlaybackStatus } from 'expo-av';
import Draggable from 'react-native-draggable';
import CodeEditor, { CodeEditorSyntaxStyles } from '@rivascva/react-native-code-editor';

import CloseSVG from '../../assets/image/icon/Close.svg';
import SoundSVG from '../../assets/image/icon/VolumeUp.svg';
import SettingSVG from '../../assets/image/icon/Setting.svg';
import questions from '../../data/questions';
import PlaySVG from '../../assets/image/icon/Play.svg';
import ButtonViewCustom from '../views/ButtonViewCustom';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';

const SampleTrack = require('../../assets/music/sounds1.mp3');

const PlayQuiz = ({ navigation, route }) => {
  // Data Dummy
  const data = questions;
  const totalQuestions = data.length;
  // points
  const [points, setPoints] = useState(0);

  // index of the question
  const [index, setIndex] = useState(0);

  // answer Status (true or false)
  const [answerStatus, setAnswerStatus] = useState(null);

  // answers
  const [answers, setAnswers] = useState([]);

  // selected answer
  const [selectedAnswerIndex, setSelectedAnswerIndex] = useState(null);

  // Counter
  const [counter, setCounter] = useState(15);

  // interval
  let interval = null;

  // progress bar
  // const progressPercentage = Math.floor((index/totalQuestions) * 100);

  useEffect(() => {
    if (selectedAnswerIndex !== null) {
      if (index < totalQuestions) {
        if (selectedAnswerIndex === currentQuestion?.correctAnswerIndex) {
          setPoints((points) => points + 10);
          setAnswerStatus(true);
          answers.push({ question: index + 1, answer: true });
        } else {
          setAnswerStatus(false);
          answers.push({ question: index + 1, answer: false });
        }
      }
    }
  }, [selectedAnswerIndex]);

  useEffect(() => {
    setSelectedAnswerIndex(null);
    setAnswerStatus(null);
  }, [index]);

  //Set counter
  // useEffect(() => {
  //   const myInterval = () => {
  //     if (counter >= 1) {
  //       setCounter((state) => state - 1);
  //     }
  //     if (counter === 0) {
  //         if(index + 1 === totalQuestions ){
  //           clearTimeout(interval)
  //           navigation.navigate("HomePlay", {
  //             answers: answers,
  //             points: points,
  //           });
  //         }else{
  //           setIndex(index + 1);
  //           // clear
  //           setCounter(15);
  //         }
  //     }
  // };

  //   interval = setTimeout(myInterval, 1000);

  //   // clean up
  //   return () => {
  //     clearTimeout(interval);
  //   };
  // }, [counter]);

  useEffect(() => {
    if (!interval) {
      setCounter(15);
    }
  }, [index]);

  const currentIndex = index;
  const currentQuestion = data[index];
  const lengthqs = currentQuestion.question.length;
  var persentaseCounter = (counter / 15) * 100;

  // Play Sound
  const [Loaded, SetLoaded] = useState(false);
  const [Loading, SetLoading] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const sound = useRef(new Audio.Sound());

  useEffect(() => {
    LoadAudio();
  }, []);

  const PlayAudio = async () => {
    try {
      const result = await sound.current.getStatusAsync();
      if (result.isLoaded) {
        if (result.isPlaying === false) {
          sound.current.playAsync();
        }
      }
    } catch (error) {}
  };

  const PauseAudio = async () => {
    try {
      const result = await sound.current.getStatusAsync();
      if (result.isLoaded) {
        if (result.isPlaying === true) {
          sound.current.pauseAsync();
        }
      }
    } catch (error) {}
  };

  const LoadAudio = async () => {
    SetLoading(true);
    const checkLoading = await sound.current.getStatusAsync();
    if (checkLoading.isLoaded === false) {
      try {
        const result = await sound.current.loadAsync(SampleTrack, {}, true);
        if (result.isLoaded === false) {
          SetLoading(false);
          console.log('Error in Loading Audio');
        } else {
          SetLoading(false);
          SetLoaded(true);
        }
      } catch (error) {
        console.log(error);
        SetLoading(false);
      }
    } else {
      SetLoading(false);
    }
  };

  const handlePlayPause = () => {
    if (!isPlaying) {
      PlayAudio();
      setIsPlaying(true);
    } else {
      PauseAudio();
      setIsPlaying(false);
    }
  };

  // Video controller
  const video = useRef(null);
  const [status, setStatus] = useState({});
  const [isPreloading, setIsPredLoading] = useState(true);

  // Fetch Data
  const [questionList, setQuestionList] = useState([]);
  const [question, setQuestion] = useState([]);

  useEffect(() => {
    fetch('https://api-admin.soca.ai/api/questions_creator/63a9147c20c52915d55d81c1')
      .then((response) => response.json())
      .then(async (data) => {
        // Ubah struktur data menjadi yang diinginkan
        const newQuestionList = await data.data.map((question, index) => ({
          id: question._id,
          options: String.fromCharCode(65 + index), // konversi dari huruf A-Z
          options: [question.option_1, question.option_2, question.option_3, question.option_4, question.option_5],
        }));
        // Simpan data ke state
        setQuestionList(newQuestionList);
        setQuestion(data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  // if(typeof questionList !== 'undefined' && questionList.length > 0){
  //   console.log(questionList);
  // }

  console.log(questionList);

  // dragable

  function handleDrag(e) {
    if (e.nativeEvent.pageX >= 300 && e.nativeEvent.pageX <= 400 && e.nativeEvent.pageY >= 600 && e.nativeEvent.pageY <= 700) {
      setIsShowing(true);
    } else {
      console.log('false');
    }
  }

  let x = 250;
  let y = 50;

  let x1 = 50;
  let y1 = 100;
  var colordata = 'blue';
  const [isShowing, setIsShowing] = useState(false);
  const [isShowing1, setIsShowing1] = useState(false);
  const [isShowing2, setIsShowing2] = useState(false);

  return (
    <ImageBackground source={require('../../assets/image/background/Background.png')} style={styles.container}>
      {/* Header */}
      {/* Timer */}
      <View style={{ backgroundColor: counter <= 7 ? '#FDBA59' : '#40BE45', height: 5, width: counter === 0 ? 0 : `${persentaseCounter}%` }}></View>
      <View style={styles.headerContainer}>
        {/* Close Icon */}
        <TouchableOpacity style={styles.languageButtonStyle}>
          <Text style={styles.languageText}>Bahasa Indonesia</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.totalQuestions}>
          <Text style={styles.languageText}>
            {index + 1}/{totalQuestions}
          </Text>
        </TouchableOpacity>

        {/* Right Icon */}
        <View style={styles.headerRightContainer}>
          <TouchableOpacity>
            <SoundSVG />
          </TouchableOpacity>
          <TouchableOpacity style={{ marginHorizontal: 6 }}>
            <SettingSVG />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.goBack()}>
            <CloseSVG />
          </TouchableOpacity>
        </View>
      </View>

      {/* <ScrollView style={styles.scrolContainer}> */}

      {/* Body Question */}

      <View
        style={{
          marginTop: 30,
          paddingHorizontal: 24,
        }}
      >
        <Text style={styles.pertanyaan}>Pertanyaan</Text>
      </View>

      <View
        style={{
          borderRadius: 30,
          paddingHorizontal: 24,
        }}
      >
        {currentQuestion.isCode ? (
          <View style={{ flexDirection: 'column', marginTop: 20 }}>
            <View style={{ height: 50, backgroundColor: '#1F1F1F' }}>
              <Text style={{ color: 'white', fontSize: 20, marginTop: 10, marginLeft: 20 }}>Code Editor</Text>
            </View>
            <CodeEditor
              style={{
                fontSize: 20,
                inputLineHeight: 26,
                highlighterLineHeight: 26,
                height: 200,
                backgroundColor: 'black',
              }}
              readOnly={true}
              initialValue={currentQuestion.code}
              language="python"
              syntaxStyle={CodeEditorSyntaxStyles.atomOneDark}
              showLineNumbers
            />
          </View>
        ) : null}
        <View style={{ flexDirection: 'column', marginTop: 10, backgroundColor: 'rgba(0, 0, 0, 0.6)', padding: 10, borderRadius: 6 }}>
          {/* Images Soekarno with center image */}
          {/* jika gambar !== null then image source jika voice */}
          {currentQuestion.jenis_pertanyaan === 'ganda' ? (
            <Image source={currentQuestion.src} style={styles.gambar}></Image>
          ) : // ===================== VIDEO =====================
          currentQuestion.jenis_pertanyaan === 'video' ? (
            <View style={styles.containerVideo}>
              <Video
                ref={video}
                style={styles.playContainerVideo}
                source={{
                  uri: 'https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
                }}
                useNativeControls
                resizeMode="contain"
                isLooping
                onLoadStart={() => setIsPredLoading(true)}
                onReadyForDisplay={() => setIsPredLoading(false)}
                onPlaybackStatusUpdate={(status) => setStatus(() => status)}
              >
                {isPreloading && <ActivityIndicator animating color={'gray'} size="large" style={{ flex: 1, position: 'absolute', top: '50%', left: '45%' }} />}
              </Video>
              <TouchableOpacity style={styles.playButtonVideo} onPress={() => (status.isPlaying ? video.current.pauseAsync() : video.current.playAsync())}>
                {isPlaying ? <CloseSVG /> : <PlaySVG />}
              </TouchableOpacity>
            </View>
          ) : //Nanti di rapihin buat component baru

          // ===================== AUDIO =====================
          currentQuestion.jenis_pertanyaan === 'audio' ? (
            // Button Play Music
            <View style={styles.containerPlay}>
              <TouchableOpacity style={styles.playButton} onPress={() => handlePlayPause()}>
                {isPlaying ? <CloseSVG /> : <PlaySVG />}
              </TouchableOpacity>
              <View style={styles.containerPlaywidth}></View>
              <Text style={{ color: '#ffffff', paddingLeft: 6, marginTop: 10 }}>0.30</Text>
            </View>
          ) : (
            <View></View>
          )}
          <Text style={{ fontSize: lengthqs >= 299 ? 14 : 18, fontWeight: 'bold', textAlign: 'center', color: '#FFFFFF' }}>{currentQuestion?.question}</Text>
        </View>

        {/*Body Answer */}
        {currentQuestion.jenis_pertanyaan === 'match' ? (
          <View style={{ height: 600 }}>
            {isShowing === true && isShowing1 === true && isShowing2 === true ? (
              setIndex(index + 1)
            ) : (
              <View>
                {isShowing ? null : (
                  <Draggable
                    imageSource={currentQuestion.src}
                    x={x1}
                    y={y1 - 50}
                    renderSize={100}
                    renderColor={colordata}
                    renderText="A"
                    isCircle
                    shouldReverse
                    onDragRelease={(e) => {
                      handleDrag(e);
                    }}
                  />
                )}
                {isShowing1 ? null : (
                  <Draggable
                    imageSource={currentQuestion.src}
                    x={x1}
                    y={y1 + 100}
                    renderSize={100}
                    renderColor={colordata}
                    renderText="A"
                    isCircle
                    shouldReverse
                    onDragRelease={(e) => {
                      if (e.nativeEvent.pageX >= 300 && e.nativeEvent.pageX <= 400 && e.nativeEvent.pageY >= 250 && e.nativeEvent.pageY <= 400) {
                        setIsShowing1(true);
                      } else {
                        console.log('false');
                      }
                    }}
                  />
                )}
                {isShowing2 ? null : (
                  <Draggable
                    imageSource={currentQuestion.src}
                    x={x1}
                    y={y1 + 250}
                    renderSize={100}
                    renderColor={colordata}
                    renderText="A"
                    isCircle
                    shouldReverse
                    onDragRelease={(e) => {
                      if (e.nativeEvent.pageX >= 300 && e.nativeEvent.pageX <= 400 && e.nativeEvent.pageY >= 450 && e.nativeEvent.pageY <= 550) {
                        setIsShowing2(true);
                      } else {
                        console.log('false');
                      }
                    }}
                  />
                )}

                {isShowing1 ? null : <Draggable x={x} y={y} renderSize={100} renderColor="rgba(0, 0, 0, 0.6)" renderText="Soekarno" disabled={true} />}
                {isShowing2 ? null : <Draggable x={x} y={y + 150} renderSize={100} renderColor="rgba(0, 0, 0, 0.6)" renderText="Soekarno" disabled={true} />}
                {isShowing ? null : <Draggable x={x} y={y + 300} renderSize={100} renderColor="rgba(0, 0, 0, 0.6)" renderText="Soekarno" disabled={true} />}
              </View>
            )}
          </View>
        ) : (
          //jenis_pertanyaan selain mencocokkan
          <>
            <Text style={{ marginTop: 30, fontSize: 14, color: '#FFFFFF' }}>Pilih Jawaban</Text>
            <SafeAreaView>
              <View style={{ marginTop: 12 }}>
                {typeof questionList[index] !== 'undefined'
                  ? questionList[index]['options'].map((item, index) => (
                      <Pressable
                        onPress={() => {
                          selectedAnswerIndex === null && setSelectedAnswerIndex(index);
                          setTimeout(() => {
                            if (currentIndex + 1 < questions.length) {
                              setIndex(currentIndex + 1);
                            }
                          }, 1000);
                        }}
                        style={selectedAnswerIndex != null && selectedAnswerIndex === index ? styles.selectedAnswer : styles.selectedDefaultAnswer}
                      >
                        <Text style={styles.TextAnswer}>{item}</Text>
                      </Pressable>
                    ))
                  : null}
              </View>
            </SafeAreaView>
          </>
        )}

        {/* Body check answer Temp */}
      </View>
      <View
        style={{
          marginTop: 45,
          padding: 10,
          borderRadius: 7,
        }}
      >
        {index + 1 == questions.length ? (
          <Pressable
            onPress={() => {
              navigation.navigate('LeaderboardCountdownScreen', { points: points, answers: answers });
            }}
            style={styles.next}
          >
            <Text style={{ color: 'white' }}>Done</Text>
          </Pressable>
        ) : null}
      </View>
    </ImageBackground>
  );
};

export default PlayQuiz;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    flex: 1,
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height + 50,
  },
  headerContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 14,
    marginRight: 19,
    marginTop: 30,
  },
  languageButtonStyle: {
    backgroundColor: 'white',
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderRadius: 17,
    alignItems: 'center',
    justifyContent: 'center',
  },
  languageText: {
    fontFamily: 'avenir-next-medium',
    fontSize: 12,
  },
  headerRightContainer: {
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
  },
  totalQuestions: {
    backgroundColor: 'white',
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderRadius: 17,
  },
  pertanyaan: {
    fontSize: 14,
    marginTop: 30,
    fontWeight: 'normal',
    color: '#FFFFFF',
  },
  containerGambar: {
    flexDirection: 'column',
    marginTop: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    padding: 10,
    borderRadius: 6,
  },
  gambar: {
    marginTop: 10,
    marginBottom: 10,
    width: 100,
    height: 100,
    alignSelf: 'center',
  },
  selectedAnswer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: '#00FFFF',
    marginVertical: 10,
    backgroundColor: 'blue',
    borderRadius: 14,
  },
  selectedDefaultAnswer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: '#000000',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    marginVertical: 10,
    borderRadius: 14,
  },
  playContainerVideo: {
    paddingTop: 20,
    marginLeft: 20,
    width: 300,
    height: 300,
    borderRadius: 20,
  },
  next: {
    backgroundColor: 'green',
    padding: 10,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 20,
    borderRadius: 6,
  },
  TextAnswer: {
    marginLeft: 20,
    fontSize: 18,
    padding: 15,
    color: 'white',
  },
  containerPlay: {
    flexDirection: 'row',
    marginTop: 10,
  },
  playButton: {
    backgroundColor: '#fffffff',
    padding: 10,
    borderRadius: 50,
  },
  playButtonVideo: {
    backgroundColor: '#fffffff',
    padding: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
  containerPlaywidth: {
    backgroundColor: '#ffffff',
    height: 3,
    width: 256,
    marginTop: 20,
  },
});
