import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableHighlight, Alert } from 'react-native';
import React, { useState } from 'react';
import LeftArrow from '../../assets/LeftArrow.svg';
import SocaLogo from '../../assets/SocaLogo.svg';
import TextInputLayout from '../views/TextInputLayout';
import ButonView from '../views/ButonView';
import textStyles from '../styles/TextStyles';
import axios from 'axios';

const baseUrl = 'https://api-landingpage.soca.ai';

const ForgotPasswordScreen = ({ navigation }) => {
  const [email, setEmail] = useState(null);
  const onEmailSendHandler = async (event) => {
    if (!email) {
      Alert.alert('Isi email kamu');
      return;
    } else {
      try {
        const response = await axios.post(`${baseUrl}/api/forgot-password`, {
          email,
        });
        if (response.status === 200) {
          if (response.data.status) {
            alert(`${JSON.stringify(response.data.message)}`);
            setEmail(null);
            navigation.navigate('otpVerificationScreen');
          } else {
            alert(`${JSON.stringify(response.data.message)}`);
          }
        } else {
          throw new Error('An error has occurred');
        }
      } catch (error) {
        alert('An error has occurred');
      }
    }
  };

  return (
    <View style={styles.mainContainer}>
      <View style={styles.header}>
        <TouchableHighlight onPress={() => navigation.navigate('signinScreen')}>
          <View style={styles.back}>
            <LeftArrow />
            <Text style={[textStyles.semiBoldText, { fontSize: 20, marginStart: 4 }]}>Kembali</Text>
          </View>
        </TouchableHighlight>
      </View>
      <View style={styles.container}>
        <SocaLogo />
        <Text style={[textStyles.semiBoldText, { fontSize: 24, marginTop: 32, marginBottom: 8 }]}>Lupa Password</Text>
        <TextInputLayout label="Email" placeholder="Masukkan email kamu" keyboardType="email-address" onChangeText={(val) => setEmail(val)} value={email} />
        <ButonView text="Kirim" onPress={() => onEmailSendHandler()} />
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerText}>Copyright © 2021 Soca.ai All Reserved</Text>
      </View>
    </View>
  );
};

export default ForgotPasswordScreen;

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'column',
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: '#000',
    marginTop: StatusBar.currentHeight || 0,
  },
  header: {
    backgroundColor: '#000',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  footer: {
    backgroundColor: '#000',
    marginBottom: 26,
  },
  back: {
    marginTop: 60,
    marginStart: 28,
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerText: {
    fontFamily: 'avenir-next-reguler',
    color: '#A6A6A6',
    fontSize: 12,
    textAlign: 'center',
  },
});
