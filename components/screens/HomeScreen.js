import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TextInput,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from "react-native";
import React, { useState, useEffect } from "react";
import LogoSmall from "../../assets/image/logo/LogoSmall.svg";
import SearchSVG from "../../assets/image/icon/Search.svg";
import NotificationSVG from "../../assets/image/icon/Notification.svg";
import UserSVG from "../../assets/image/icon/User.svg";
import PlaySVG from "../../assets/image/icon/Play.svg";
import QuestionSVG from "../../assets/image/icon/Question.svg";
import StarLikedSVG from "../../assets/image/icon/StarLiked.svg";
import StarUnlikedSVG from "../../assets/image/icon/StarUnliked.svg";
import CalenderSVG from "../../assets/image/icon/Calendar.svg";
import TopBarView from "../views/TopBarView";
import Animated, { SlideInDown, SlideOutDown } from "react-native-reanimated";
import axios from "axios";

const HomeScreen = ({ navigation }) => {
  const [dataAtHome, setDataAtHome] = useState([
    {
      id: 1,
      desc: "Science itu menyenangkan dan mengasyikan",
      image: require("../../assets/image/avatar/Male-Scientist.png"),
      people: 99,
    },
    {
      id: 2,
      desc: "Bekal untuk menjadi seorang Software Engineer",
      image: require("../../assets/image/avatar/Male-SoftwareEngineer.png"),
      people: 99,
    },
  ]);

  // const [dataQuiz, setDataQuiz] = useState([
  //   {
  //     id: 1,
  //     title: "Sistem Pemasaran Dasar",
  //     playTime: 320,
  //     question: 10,
  //     like: 200,
  //     image: require("../../assets/image/logo/PemasaranPenjualan.png"),
  //     liked: false,
  //   },
  //   {
  //     id: 2,
  //     title: "Simple Present Dasar",
  //     playTime: 320,
  //     question: 10,
  //     like: 200,
  //     image: require("../../assets/image/logo/BahasaInggris.png"),
  //     liked: true,
  //   },
  //   {
  //     id: 3,
  //     title: "Bahasa Indonesia Dasar",
  //     playTime: 320,
  //     question: 10,
  //     like: 200,
  //     image: require("../../assets/image/logo/BahasaIndonesia.png"),
  //     liked: false,
  //   },
  //   {
  //     id: 4,
  //     title: "Sistem Pemasaran Dasar",
  //     playTime: 320,
  //     question: 10,
  //     like: 200,
  //     image: require("../../assets/image/logo/PemasaranPenjualan.png"),
  //     liked: false,
  //   },
  // ]);

  const [dataAdventure, setDataAdventure] = useState([
    {
      id: 1,
      title: "Menganalisa gunung berapi yang masih aktif",
      image: require("../../assets/image/icon/Volcano.png"),
      user: 150,
      date: "3 Dec",
    },
    {
      id: 2,
      title: "Modifikasi motor di Babe Garage",
      image: require("../../assets/image/icon/Volcano.png"),
      user: 99,
      date: "3 Dec",
    },
  ]);

  const [dailyQuiz, setDailyQuiz] = useState()
  const getDataDailyQuiz = async () => {
    try {
      const res = await axios.get('https://api-admin.soca.ai/api/quiz/all', {
        params:{
          page: 1,
          limit: 10
        }
      });
      setDailyQuiz(res.data.data)
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getDataDailyQuiz();
  }, [])

  return (
    <SafeAreaView style={styles.container}>
      {/* Header */}
      {console.log(dailyQuiz)}
      <TopBarView />
      {/* <View style={styles.headerContainer}>
        <LogoSmall />

        <View style={styles.headerRightItemContainer}>
          <SearchSVG style={{ marginRight: 22 }} />
          <NotificationSVG />
        </View>
      </View> */}

      <ScrollView style={styles.scrolContainer}>
        {/* Search bar */}
        <Animated.View
          style={styles.searcBarContainer}
          entering={SlideInDown.duration(500)}
          exiting={SlideOutDown.duration(500)}
        >
          <SearchSVG
            style={{ marginLeft: 15, marginTop: 11, marginBottom: 10 }}
          />
          <TextInput
            placeholder="Search"
            style={styles.searchTextInputStyle}
            placeholderTextColor="#8C8C8C"
          />
        </Animated.View>

        {/* Card */}
        <Animated.View
          style={styles.bigCardContainer}
          entering={SlideInDown.duration(1000)}
          exiting={SlideOutDown.duration(1000)}
        >
          <Image
            source={require("../../assets/image/avatar/Male-Doctor.png")}
            style={{ alignSelf: "flex-end", marginTop: 27, marginLeft: 9 }}
          />
          <View style={styles.bigCardRightContentContainer}>
            <Text style={styles.bigCardTitleText}>
              Hi, you want to be a Doctor?
            </Text>
            <Text style={styles.bigCardDescText}>
              Before you take a risk decision, let dive in to learn the journey.
            </Text>
            <TouchableOpacity style={styles.learnButtonStyle}>
              <Text style={styles.learnButtonText}>Learn now</Text>
            </TouchableOpacity>
          </View>
        </Animated.View>

        {/* Everything at home */}
        <Animated.Text
          style={styles.HomeText}
          entering={SlideInDown.duration(1500)}
          exiting={SlideOutDown.duration(1500)}
        >
          Everything at home
        </Animated.Text>

        {/* List Everything At Home*/}
        <FlatList
          style={{ flexGrow: 0, marginTop: 10, marginLeft: 24, marginBottom: 15 }}
          horizontal={true}
          data={dataAtHome}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <Animated.View
              style={styles.cardAtHomeContainer}
              entering={SlideInDown.duration(2000)}
              exiting={SlideOutDown.duration(2000)}
            >
              <View style={styles.cardAtHomeLeftSideContainer}>
                <Image source={item.image} />

                <Text style={styles.atHomeLeftSideText}>{item.desc}</Text>
              </View>
              <View style={styles.cardAtHomeRightSideContainer}>
                <View style={styles.cardAthHomeUserContainer}>
                  <UserSVG style={{ marginRight: 3 }} />
                  <Text style={styles.userText}>{item.people}</Text>
                </View>
                <TouchableOpacity style={styles.joinButtonStyle}>
                  <Text style={styles.joinButtonText}>Join</Text>
                </TouchableOpacity>
              </View>
            </Animated.View>
          )}
        />

        {/* Daily Quiz */}
        <Animated.Text
          style={styles.HomeText}
          entering={SlideInDown.duration(2500)}
          exiting={SlideOutDown.duration(2500)}
        >
          Daily quizzes
        </Animated.Text>

        {/* List Quiz */}
        <FlatList
          style={{ flexGrow: 0, marginTop: 7, marginLeft: 24, height: 128, marginBottom: 15 }}
          horizontal={true}
          data={dailyQuiz}
          keyExtractor={(item) => item._id}
          renderItem={({ item }) => (
            <Animated.View
              style={styles.quizContainer}
              entering={SlideInDown.duration(3000)}
              exiting={SlideOutDown.duration(3000)}
            >
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("HomePlay", item._id);
                }}
              >
                <Image source={{uri:item.images_cover}} width={103.58} height={74.63}/>
              </TouchableOpacity>
              <Text style={styles.quizTitleText}>{item.quiz_name}</Text>
              <View style={styles.quizFooterContainer}>
                <View style={styles.quizFooter}>
                  <PlaySVG />
                  <Text style={styles.quizText}>{item.total_play}</Text>
                </View>
                <View style={styles.quizFooter}>
                  <QuestionSVG />
                  <Text style={styles.quizText}>{item.total_question}</Text>
                </View>
                <View style={styles.quizFooter}>
                  {false ? <StarLikedSVG /> : <StarUnlikedSVG />}
                  <Text style={styles.quizText}>{item.rating}</Text>
                </View>
              </View>
            </Animated.View>
          )}
        />

        {/* More Adventure */}
        <Animated.Text
          style={styles.HomeText}
          entering={SlideInDown.duration(3500)}
          exiting={SlideOutDown.duration(3500)}
        >
          More adventure
        </Animated.Text>

        {/* List More Adventure */}
        <FlatList
          style={{ flexGrow: 0, marginTop: 10, marginLeft: 24 }}
          horizontal={true}
          data={dataAdventure}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <Animated.View
              style={styles.adventureContainer}
              entering={SlideInDown.duration(4000)}
              exiting={SlideOutDown.duration(4000)}
            >
              <View style={styles.adventureLeftContainer}>
                <Text style={styles.adventureTitleText}>{item.title}</Text>
                <View style={styles.adventureFooterContainer}>
                  <View style={styles.adventureFooter}>
                    <UserSVG />
                    <Text style={styles.adventureText}>{item.user}</Text>
                  </View>
                  <View style={styles.adventureFooter}>
                    <CalenderSVG />
                    <Text style={styles.adventureText}>{item.date}</Text>
                  </View>
                </View>
              </View>
              <Image
                source={item.image}
                style={{
                  marginTop: 16,
                  marginRight: 11,
                  width: 40,
                  height: 40,
                }}
              />
            </Animated.View>
          )}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  scrolContainer: {
    flex: 1,
    backgroundColor: "black",
  },
  container: {
    flex: 1,
    backgroundColor: "black",
  },
  headerContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginLeft: 24,
    marginTop: 24,
    marginRight: 30,
    marginBottom: 24,
  },
  headerRightItemContainer: {
    flexDirection: "row",
  },
  searcBarContainer: {
    flexDirection: "row",
    backgroundColor: "#222222",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: 10,
    marginTop: 23,
    marginLeft: 25,
    marginRight: 24,
    borderRadius: 4,
  },
  searchTextInputStyle: {
    marginLeft: 11,
    color: "white",
    fontFamily: "avenir-next-demibold",
    width: "90%",
    fontSize: 12,
  },
  bigCardContainer: {
    flexDirection: "row",
    backgroundColor: "#222222",
    alignContent: "flex-end",
    justifyContent: "flex-start",
    marginLeft: 24,
    marginTop: 26,
    marginRight: 25,
    borderRadius: 10,
    marginBottom: 15,
  },
  bigCardRightContentContainer: {
    marginRight: 19,
    width: 208,
  },
  bigCardTitleText: {
    fontFamily: "avenir-next-medium",
    color: "white",
    width: 200,
    fontSize: 16,
    marginRight: 33,
    marginTop: 27,
  },
  bigCardDescText: {
    color: "#9D9D9D",
    width: 172,
    fontFamily: "avenir-next-reguler",
    fontSize: 12,
    marginTop: 7,
  },
  learnButtonStyle: {
    backgroundColor: "#BC3BF2",
    alignSelf: "flex-end",
    justifyContent: "center",
    borderRadius: 22,
    marginRight: 19,
    marginTop: 14,
    marginBottom: 15,
  },
  learnButtonText: {
    color: "white",
    paddingVertical: 5,
    paddingRight: 10,
    paddingLeft: 12,
    fontSize: 10,
    fontFamily: "avenir-next-demibold",
  },
  HomeText: {
    fontSize: 14,
    fontFamily: "avenir-next-reguler",
    color: "white",
    marginTop: 15,
    marginLeft: 24,
  },
  cardAtHomeContainer: {
    backgroundColor: "#222222",
    flexDirection: "row",
    marginLeft: 8,
    borderRadius: 10,
  },
  cardAtHomeLeftSideContainer: {
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginTop: 15,
    marginLeft: 17,
  },
  atHomeLeftSideText: {
    fontSize: 10,
    fontFamily: "avenir-next-reguler",
    color: "white",
    marginTop: 7,
    marginBottom: 15,
    width: 123,
  },
  cardAtHomeRightSideContainer: {
    marginRight: 12,
    marginTop: 14,
    alignItems: "flex-end",
    justifyContent: "space-between",
  },
  cardAthHomeUserContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  userText: {
    color: "white",
    fontFamily: "avenir-next-reguler",
    fontSize: 8,
  },
  joinButtonStyle: {
    backgroundColor: "#BC3BF2",
    alignSelf: "flex-end",
    justifyContent: "center",
    marginBottom: 11,
    borderRadius: 22,
  },
  joinButtonText: {
    fontFamily: "avenir-next-medium",
    fontSize: 8,
    color: "white",
    paddingHorizontal: 12,
    paddingVertical: 3,
  },
  quizContainer: {
    marginLeft: 6,
    borderRadius: 5,
    backgroundColor: "#222222",
    justifyContent: 'space-evenly'
  },
  quizTitleText: {
    fontFamily: "avenir-next-demibold",
    color: "white",
    fontSize: 9,
    marginLeft: 7,
    marginTop: 6,
    width: 76,
  },
  quizFooterContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginLeft: 7.48,
    marginRight: 9.58
  },
  quizFooter: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 6,
    marginBottom: 9,
  },
  quizText: {
    color: "#C1C1C1",
    fontSize: 6,
    fontFamily: "avenir-next-reguler",
    marginLeft: 2,
  },
  adventureContainer: {
    flexDirection: "row",
    marginTop: 10,
    marginLeft: 9,
    backgroundColor: "#222222",
    borderRadius: 10,
    marginBottom: 84,
  },
  adventureLeftContainer: {
    marginTop: 26,
    marginLeft: 23,
    marginBottom: 17,
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  adventureTitleText: {
    fontFamily: "avenir-next-reguler",
    fontSize: 14,
    color: "white",
    width: 181,
  },
  adventureFooterContainer: {
    marginTop: 20,
    flexDirection: "row",
    alignItem: "center",
    justifyContent: "flex-start",
  },
  adventureFooter: {
    flexDirection: "row",
    alignItem: "center",
    justifyContent: "center",
  },
  adventureText: {
    color: "#C1C1C1",
    fontSize: 10,
    fontFamily: "avenir-next-reguler",
    marginLeft: 3,
    marginRight: 11,
  },
});
