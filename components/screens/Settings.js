import {
    StyleSheet,
    Text,
    View,
    ImageBackground,
    TouchableOpacity,
    TextInput,
    Dimensions,
    Switch,
    FlatList,
    Image,
    Alert 
  } from "react-native";
  import React, { useState, useEffect } from "react";
  import { Audio } from "expo-av";

  import CloseSVG from "../../assets/image/icon/Close.svg";
  import SoundSVG from "../../assets/image/icon/VolumeUp.svg";

  import ButtonViewCustom from '../views/ButtonViewCustom';
  const soundObject = new Audio.Sound();
  
  const Settings = ({ navigation, route }) => {
    const [quizId, setQuizId] = useState(route.id);
    const [username, setUsername] = useState(null)
    const [isEnabled, setIsEnabled] = useState(false);
    const [indexSpace, setIndexSpace] = useState(0);
    const [indexMusic, setIndexMusic] = useState(0);
    const [selectedIndex, setSelectedIndex] = useState(null);
  
    // modal status
    const [modalVisibleStatus, setModalVisibleStatus] = useState(true);
  
    const [dataSpace, setDataSpace] = useState([
      {
        id: 0,
        name: "Astronot",
        image: require("../../assets/image/background/Bg-Space.png"),
        bg : require("../../assets/image/background/Background.png"),
      },
      {
        id: 1,
        name: "Camping",
        image: require("../../assets/image/background/Bg-Camp.png"),
        bg : require("../../assets/image/background/Bg-Camp.png"),
      },
      {
        id: 2,
        name: "Cafe",
        image: require("../../assets/image/background/Bg-Cafe.png"),
        bg : require("../../assets/image/background/Bg-Cafe.png"),
      },
      {
        id: 3,
        name: "Beach",
        image: require("../../assets/image/background/Bg-Beach.png"),
        bg : require("../../assets/image/background/Bg-Lake.png"),
      },
      {
        id: 4,
        name: "Air Terjun",
        image: require("../../assets/image/background/Bg-Lake.png"),
        bg : require("../../assets/image/background/Bg-Lake.png"),
      },
    ]);
  
    const [dataMusic, setDataMusic] = useState([
      {
        id: 0,
        name: "Metal",
        image: require("../../assets/image/background/Metal.png"),
        music : require('../../assets/music/sounds1.mp3')
      },
      {
        id: 1,
        name: "Classic",
        image: require("../../assets/image/background/Classical.png"),
        music : require('../../assets/music/sounds2.mp3')
      },
      {
        id: 2,
        name: "Rock",
        image: require("../../assets/image/background/Rock.png"),
        music : require('../../assets/music/sounds2.mp3')
      },
      {
        id: 3,
        name: "Beat",
        image: require("../../assets/image/background/Beat.png"),
        music : require('../../assets/music/sounds1.mp3')
      },
      {
        id: 4,
        name: "Techno",
        image: require("../../assets/image/background/Techno.png"),
        music : require('../../assets/music/sounds1.mp3')
      },
    ]);
    
    var dataImage = dataSpace[indexSpace].bg;
    var dataMusik = dataMusic[indexMusic].music;
    
    const [sound, setSound] = useState();
  
    async function playSound() {
      console.log('Loading Sound');
  
      const { sound } = await Audio.Sound.createAsync(
        dataMusik
      );
  
      setSound(sound);
  
      console.log('Playing Sound');
      await sound.playAsync(); 
    }
  
    async function stopSound() {
          if (!sound) { return; } 
          console.log('Stopping Sound');
          await sound.stopAsync();
          await sound.unloadAsync();
    }

    async function loopAudio() {
        soundObject.setIsLoopingAsync(true); // Set looping menjadi aktif
        try {
           await soundObject.replayAsync();
        } catch (error) {
           console.log('Error: ', error);
        }
     }

    return (
      <ImageBackground
      source={dataImage}
        style={styles.container}
      >
        {/* Header */}
        <View style={styles.headerContainer}>
          {/* Close Icon */}
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <CloseSVG />
          </TouchableOpacity>
  
          {/* Volume Icon */}
          <TouchableOpacity>
            <SoundSVG />
          </TouchableOpacity>
        </View>

  
        {/* Settings */}
        <Text style={styles.titleText}>Pengaturan</Text>
  
        <View style={styles.settingsContainer}>
          <Text style={styles.settingsText}>Audio</Text>
          <Switch 
         trackColor={{ false: "#767577", true: "#81b0ff" }}
         thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
         ios_backgroundColor="#3e3e3e"
         onValueChange={(value) => {
           setIsEnabled(value)
           if (value) {
             playSound();
             loopAudio();
           } else {
             stopSound();
           }             
         }}
         value={isEnabled}
       />
         
        </View>
  
        {/* Space */}
        <View style={styles.spaceContainer}>
          {/* Choose Space */}
          <Text style={styles.settingsText}>Pilih Space</Text>
  
          {/* List Space */}
          <FlatList
            style={{ flexGrow: 0, marginTop: 10 }}
            horizontal={true}
            data={dataSpace}
            renderItem={({ item }) => (
                console.log(item.id),
              <View style={styles.cardSpaceContainer}>
                <TouchableOpacity
                  onPress={() => {
                    setIndexSpace(item.id);
                  }}
                >
                  <Image
                    source={item.image}
                    style={{
                      borderWidth: indexSpace === item.id ? 1.5 : 0,
                      borderColor: "#40BE45",
                    }}
                  />
                  <Text style={styles.spaceText}>{item.name}</Text>
                </TouchableOpacity>
              </View>
            )}
          />
  
          {/* Music */}
          <Text style={styles.musicTitleText}>Pilih Music</Text>
  
          {/* List Music */}
          <FlatList
            style={{ flexGrow: 0, marginTop: 11 }}
            horizontal={true}
            data={dataMusic}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => (
              <View style={styles.cardSpaceContainer}>
                <TouchableOpacity
                  onPress={() => {
                    setIndexMusic(item.id);
                  }}
                >
                  <Image
                    source={item.image}
                    style={{
                      borderWidth: indexMusic === item.id ? 1.5 : 0,
                      borderColor: "#40BE45",
                    }}
                  />
                  <Text style={styles.spaceText}>{item.name}</Text>
                </TouchableOpacity>
              </View>
            )}
          />
        </View>
        
        {/* Button Simpan */}
       <View style={{marginTop: (Dimensions.get('window').width / 2)}}>
        <ButtonViewCustom 
            text="Simpan" onPress={()=>{
                Alert.alert("Simpan pengaturan sukses !!!")
            }} backgroundColor="#40BE45" />
       </View>
      </ImageBackground>
    );
  };
  


  export default Settings;
  
  const styles = StyleSheet.create({
    container: {
      position: "absolute",
      flex: 1,
      left: 0,
      top: 0,
      bottom: 0,
      right: 0,
      width: Dimensions.get("window").width,
      height: Dimensions.get("window").height + 50,
    },
    headerContainer: {
      alignItems: "center",
      flexDirection: "row",
      justifyContent: "space-between",
      marginLeft: 31,
      marginRight: 31,
      marginTop: 30,
    },
    nameContainer: {
      backgroundColor: "#1B1B1B",
      marginHorizontal: 31,
      marginTop: 37,
      paddingHorizontal: 27,
      paddingVertical: 16,
    },
    nameTitleText: {
      fontFamily: "avenir-next-reguler",
      fontSize: 12,
      color: "white",
    },
    nameTextInputStyle: {
      marginTop: 7,
      backgroundColor: "white",
      padding: 8,
      borderRadius: 2,
      marginBottom: 11,
    },
    submitNameButtonStyle: {
      backgroundColor: "#6D75F6",
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 4,
      paddingVertical: 8,
    },
    submitNameText: {
      color: "white",
      fontFamily: "avenir-next-medium",
      fontSize: 14,
    },
    titleText: {
      fontFamily: "avenir-next-demibold",
      fontSize: 14,
      color: "white",
      marginLeft: 31,
      marginTop: 50,
    },
    settingsContainer: {
      backgroundColor: "#1B1B1B",
      alignItems: "center",
      justifyContent: "space-between",
      flexDirection: "row",
      paddingHorizontal: 27,
      paddingVertical: 16,
      marginHorizontal: 31,
      borderRadius: 8,
    },
    settingsText: {
      fontFamily: "avenir-next-reguler",
      fontSize: 12,
      color: "white",
    },
    spaceContainer: {
      marginTop: 18,
      backgroundColor: "#1B1B1B",
      paddingLeft: 31,
      paddingTop: 18,
      paddingBottom: 20,
    },
    cardSpaceContainer: {
      marginLeft: 6,
      alignItems: "center",
      justifyContent: "center",
    },
    spaceText: {
      fontFamily: "avenir-next-reguler",
      color: "white",
      fontSize: 10,
      alignSelf: "center",
      marginTop: 7,
    },
    musicTitleText: {
      fontFamily: "avenir-next-demibold",
      fontSize: 12,
      color: "white",
      marginTop: 15,
    },
  });
  