import { StyleSheet, Text, View, ImageBackground, Dimensions, TouchableOpacity, Image, FlatList } from 'react-native';
import React, { useState } from 'react';
import { BlurView } from 'expo-blur';
import CloseSVG from '../../assets/image/icon/Close.svg';
import SoundSVG from '../../assets/image/icon/VolumeUp.svg';
import SettingSVG from '../../assets/image/icon/Setting.svg';
import CountSVG from '../../assets/image/icon/CountUser.svg';
import Animated, { SlideInDown, SlideOutDown } from 'react-native-reanimated';
import translations from '../../localization';
// import * as Localization from "expo-localization";
import { I18n } from 'i18n-js';

const LobbySeruan = ({ navigation, route }) => {
  //set bahasa
  const i18n = new I18n(translations);
  i18n.locale = 'eng';
  i18n.enableFallback = true;

  //bahasa
  const [locale, setLocale] = useState(i18n.locale);
  const changeLocale = (locale) => {
    i18n.locale = locale;
    setLocale(locale);
  };

  const [username, setUSername] = useState(route.params.username);
  const [background, setBackground] = useState(route.params.background);
  const [dataPlayer, setDataPlayer] = useState([
    {
      id: 1,
      name: 'Asep Balon',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 2,
      name: 'Dadang Karbit',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 3,
      name: 'Markus Horison',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 4,
      name: 'Wawan Gunawan',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 5,
      name: 'Asep Balon',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 6,
      name: 'Dadang Karbit',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 7,
      name: 'Markus Horison',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 8,
      name: 'Wawan Gunawan',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 9,
      name: 'Asep Balon',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 10,
      name: 'Dadang Karbit',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 11,
      name: 'Markus Horison',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 12,
      name: 'Wawan Gunawan',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 13,
      name: 'Asep Balon',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 14,
      name: 'Dadang Karbit',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 15,
      name: 'Markus Horison',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 16,
      name: 'Wawan Gunawan',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 17,
      name: 'Asep Balon',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 18,
      name: 'Dadang Karbit',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 19,
      name: 'Markus Horison',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 20,
      name: 'Wawan Gunawan',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 21,
      name: 'Dadang Karbit',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 22,
      name: 'Markus Horison',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
    {
      id: 23,
      name: 'Wawan Gunawan',
      image: require('../../assets/image/avatar/DadangKarbit.png'),
    },
  ]);

  //timer
  const [timerCount, setTimer] = useState(5);

  useEffect(() => {
    let interval = setInterval(() => {
      setTimer((lastTimerCount) => {
        lastTimerCount <= 1 && clearInterval(interval);
        return lastTimerCount - 1;
      });
    }, 1000); //each count lasts for a second
    //cleanup the interval on complete
    return () => clearInterval(interval);
  }, []);

  return (
    <ImageBackground source={background} style={styles.container}>
      {timerCount == 0
        ? navigation.replace('LeaderboardScreen', {
            background: background,
          })
        : ''}
      {/* Header */}
      <View style={styles.headerContainer}>
        {/* Close Icon */}
        <TouchableOpacity style={styles.languageButtonStyle}>
          <Text style={styles.languageText}>{i18n.t('language')}</Text>
        </TouchableOpacity>

        {/* Right Icon */}
        <View style={styles.headerRightContainer}>
          <TouchableOpacity>
            <SoundSVG />
          </TouchableOpacity>
          <TouchableOpacity style={{ marginHorizontal: 6 }}>
            <SettingSVG />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.goBack()}>
            <CloseSVG />
          </TouchableOpacity>
        </View>
      </View>

      {/* <ScrollView style={styles.scrolContainer}> */}

      {/* Body Container */}
      <Animated.View style={styles.bodyContainer} entering={SlideInDown.duration(1000)} exiting={SlideOutDown.duration(1000)}>
        {/* User */}
        <BlurView intensity={80} tint="dark" style={styles.userContainer}>
          <Image source={require('../../assets/image/avatar/User.png')} style={{ marginLeft: 26, marginTop: 10, marginBottom: 10 }} />
          <View style={styles.userInsideContainer}>
            <Text style={styles.nameText}>{username}</Text>
          </View>
        </BlurView>

        {/* Player Container */}
        <BlurView intensity={80} tint="dark" style={styles.playerContainer}>
          <View style={styles.bodyHeaderContainer}>
            <CountSVG style={{ marginTop: 27 }} />
            <Text style={styles.countText}>
              {dataPlayer.length} {i18n.t('participant')}
            </Text>
          </View>

          {/* Waiting Text */}
          <Text style={styles.waitingText}>{i18n.t('waitingText')}</Text>

          {/* List Player */}
          <FlatList
            style={{ marginTop: 19, flexGrow: 0, marginBottom: 39 }}
            numColumns={2}
            data={dataPlayer}
            ListEmptyComponent={<Text style={styles.emptyUserText}>Menunggu Peserta ...</Text>}
            renderItem={({ item }) => (
              <BlurView intensity={100} tint="dark" style={styles.specificPlayerContainer}>
                <Image
                  source={item.image}
                  style={{
                    marginVertical: 17,
                    marginLeft: 18,
                    marginRight: 11,
                  }}
                />
                <Text style={styles.listNameText}>{item.name}</Text>
              </BlurView>
            )}
          />
        </BlurView>
      </Animated.View>
    </ImageBackground>
  );
};

export default LobbySeruan;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    flex: 1,
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height + 50,
    paddingBottom: 105,
  },
  headerContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 14,
    marginRight: 19,
    marginTop: 45,
  },
  languageButtonStyle: {
    backgroundColor: 'white',
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderRadius: 17,
    alignItems: 'center',
    justifyContent: 'center',
  },
  languageText: {
    fontFamily: 'avenir-next-medium',
    fontSize: 12,
  },
  headerRightContainer: {
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
  },
  bodyContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 15,
    marginTop: 152,
    marginBottom: 80,
    height: 582,
  },
  userContainer: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderRadius: 14,
    borderWidth: 0.88,
    alignSelf: 'stretch',
  },
  userInsideContainer: {
    marginLeft: 17,
    alignItems: 'flex-start',
    marginVertical: 25,
  },
  nameText: {
    color: 'white',
    fontSize: 18,
    fontFamily: 'avenir-next-medium',
  },
  playerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 13,
    borderRadius: 14,
    borderWidth: 0.88,
    alignSelf: 'stretch',
  },
  bodyHeaderContainer: {
    alignSelf: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 19,
    justifyContent: 'center',
  },
  countText: {
    marginTop: 31,
    marginLeft: 7,
    color: 'white',
    fontFamily: 'avenir-next-medium',
    fontSize: 12,
  },
  waitingText: {
    color: 'white',
    fontSize: 14,
    fontFamily: 'avenir-next-reguler',
    marginTop: 20,
    marginBottom: 5,
  },
  emptyUserText: {
    color: '#B7B7B7',
    fontSize: 10,
    fontFamily: 'avenir-next-reguler',
    marginTop: 28,
  },
  specificPlayerContainer: {
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderRadius: 8.44,
    borderWidth: 0.74,
    marginBottom: 13,
  },
  listNameText: {
    color: 'white',
    fontFamily: 'avenir-next-medium',
    width: 84,
    fontSize: 12,
    marginLeft: 10,
    marginRight: 19,
  },
});
