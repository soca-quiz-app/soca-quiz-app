import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  TextInput,
} from "react-native";
import React, { useState } from "react";
import SocaLogo from "../../assets/SocaLogo.svg";
import SearchIcon from "../../assets/Search.svg";
import CloseSVG from "../../assets/image/icon/Close.svg";
import NotificationIcon from "../../assets/Notification.svg";
import Animated, { SlideInRight, SlideOutRight } from "react-native-reanimated";

const TopBarView = () => {
  const [expanded, setExpanded] = useState(false);

  return (
    <View style={styles.container}>
      {!expanded && <SocaLogo width={87.44} height={21.35} />}
      <View style={styles.iconAction}>
        {!expanded && (
          <TouchableWithoutFeedback onPress={() => setExpanded(true)}>
            <SearchIcon style={{ marginEnd: 22 }} />
          </TouchableWithoutFeedback>
        )}
        {expanded && (
          <Animated.View
            style={styles.searcBarContainer}
            entering={SlideInRight.duration(500)}
            exiting={SlideOutRight.duration(100)}
          >
            <TouchableWithoutFeedback onPress={() => setExpanded(false)}>
              <SearchIcon
                width={15}
                height={15}
                style={{ marginLeft: 15, marginTop: 11, marginBottom: 10 }}
              />
            </TouchableWithoutFeedback>

            <TextInput
              placeholder="Search"
              style={styles.searchTextInputStyle}
              placeholderTextColor="#8C8C8C"
            />
            <TouchableWithoutFeedback onPress={() => setExpanded(false)}>
              <CloseSVG width={15} height={15} style={{ marginRight: 15 }} />
            </TouchableWithoutFeedback>
          </Animated.View>
        )}

        {!expanded && (
          <View style={{ flexDirection: "row" }}>
            <NotificationIcon />
            <View style={[styles.notifAlert, { opacity: 1 }]}>
              <Text style={styles.notifAlertText}>1</Text>
            </View>
          </View>
        )}
      </View>
    </View>
  );
};

export default TopBarView;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginTop: 45,
    backgroundColor: "#000",
    margin: 24,
    height: 30,
    justifyContent: "space-between",
  },
  iconAction: {
    flexDirection: "row",
    alignSelf: "flex-start",
  },
  notifAlert: {
    marginLeft: -9,
    marginTop: -6,
    backgroundColor: "#EB5757",
    borderRadius: 100,
    fontSize: 6,
    width: 13,
    height: 13,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
  },
  notifAlertText: {
    color: "white",
    fontSize: 6,
    fontFamily: "avenir-next-bold",
  },
  searcBarContainer: {
    flexDirection: "row",
    backgroundColor: "#222222",
    alignSelf: 'stretch',
    alignItems: "center",
    justifyContent: "space-evenly",
    marginRight: 24,
    borderRadius: 8,
  },
  searchTextInputStyle: {
    marginLeft: 11,
    color: "white",
    width: "80%",
    marginRight: 11,
    fontFamily: "avenir-next-demibold",
    fontSize: 12,
  },
});
