import { StyleSheet, Text, TouchableHighlight } from 'react-native';
import React from 'react';

const ButtonViewCustom = ({ 
    text = '', 
    onPress = null,
    ...stylecontainer
 }) => {
    console.log(Object.is(stylecontainer))
    console.log("Value Is : " + Object.keys(stylecontainer))
    console.log(stylecontainer["color"]);
    
  return (
    <TouchableHighlight style=
    {
        Object.keys(stylecontainer).length > 0 ? {
            flexDirection: 'row',
            backgroundColor: '#6D75F6',
            // padding: 16,
            height:50,
            borderRadius: 10,
            marginTop: 20,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'stretch',
            marginHorizontal: 38,
            ...stylecontainer,
        } : styles.container
    }
    
    onPress={onPress}>
      <Text style={
        
            stylecontainer["color"] === undefined ? styles.title :
            {
                fontFamily: 'avenir-next-demibold',
                color: '#fff',
                fontWeight: '500',
                fontSize: 14,
                color: stylecontainer["color"],
            }
        
      }>{text}</Text>
    </TouchableHighlight>
  );
};

export default ButtonViewCustom;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#6D75F6',
    // padding: 16,
    height:50,
    borderRadius: 10,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    marginHorizontal: 38,
  },
  title: {
    fontFamily: 'avenir-next-demibold',
    color: '#fff',
    fontWeight: '500',
    fontSize: 14,
  },
});