import { StyleSheet, Text, View, Modal, TouchableOpacity } from 'react-native';
import React, { useState, useEffect } from 'react';
import { BlurView } from 'expo-blur';
import ContinueGoogleButton from './ContinueGoogleButton';
import * as WebBrowser from 'expo-web-browser';
import * as Google from 'expo-auth-session/providers/google';
import { useNavigation } from '@react-navigation/native';

WebBrowser.maybeCompleteAuthSession();

const PlayModal = ({
  modalVisible,
  handleOnClose,
  handleOnSignIn,
  handleOnSkip,
}) => {
  const [accessToken, setAccessToken] = useState(null);
  const [request, response, promptAsync] = Google.useIdTokenAuthRequest({
    clientId: '159695980112-ci1f9navcet8pk0ug6iornh9t5eokt1u.apps.googleusercontent.com',
    iosClientId: '159695980112-3qc8qjgn7ls3114d95lpmj10i3ceeuul.apps.googleusercontent.com',
    androidClientId: '159695980112-jmq8e2afb9p4okjiogdcu09v397htodl.apps.googleusercontent.com',
  });

  useEffect(() => {
    if (response?.type === 'success') {
      setAccessToken(response.authentication.accessToken);
      accessToken && handleOnSkip();
    }
  }, [response, accessToken]);

  return (
    <Modal animationType="slide" transparent={true} visible={modalVisible} onRequestClose={handleOnClose} style={styles.blur}>
      <BlurView intensity={100} tint="dark" style={styles.modalContainer}>
        <View style={styles.bodyContainer}>
          <Text style={styles.textStyle}>Please sign in using a Google account</Text>
          <ContinueGoogleButton text="Continue with Google" borderRadius={25} onPress={() => promptAsync()} statate={!request} />
          <TouchableOpacity onPress={handleOnSkip}>
            <Text style={styles.skipTextStyle}>Skip</Text>
          </TouchableOpacity>
        </View>
      </BlurView>
    </Modal>
  );
};

export default PlayModal;

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  blur: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  bodyContainer: {
    height: 235,
    width: 414,
    backgroundColor: '#1F1F1F',
    alignContent: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
  },
  textStyle: {
    alignSelf: 'center',
    color: 'white',
    fontFamily: 'avenir-next-reguler',
    fontSize: 18,
    marginBottom: 35,
    marginTop: 50,
  },
  googleButtonStyle: {
    backgroundColor: 'white',
    alignContent: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  buttonText: {
    fontFamily: 'avenir-next-reguler',
    fontSize: 15,
    borderRadius: 25,
  },
  skipTextStyle: {
    alignSelf: 'center',
    fontFamily: 'avenir-next-reguler',
    fontSize: 17,
    color: '#8C8C8C',
    marginTop: 21,
    marginBottom: 33,
  },
});
