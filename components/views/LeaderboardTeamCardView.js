import { StyleSheet, Text, TouchableHighlight, Image, } from "react-native";
import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { FlatList } from "react-native-gesture-handler";
import { View } from 'react-native';
import GoldMedalSVG from '../../assets/image/icon/GoldMedal.svg';
import SilverMedalSVG from '../../assets/image/icon/SilverMedal.svg';
import BronzeMedalSVG from '../../assets/image/icon/BronzeMedal.svg';
import LeaderboardPlayerCardViewMini from "./LeaderboardPlayerCardViewMini";

const LeaderboardTeamCardView = ({
    dataPlayers = [],
    namaTim = "Test Tim",
    timprofilepic = require('../../assets/image/avatar/Wolf.png'),
    rankTim = 0,
    pointim = 30,
}) => {
    // console.log(Object.is(stylecontainer))
    // console.log("Value Is : " + Object.keys(stylecontainer))
    // console.log(stylecontainer["color"]);

    return (

        <SafeAreaView style={styles.container}>
            <View style={styles.horizontalcontainer}>
                {
                    rankTim > 0 ? <Text style={styles.textStyle}>#{rankTim}</Text> : null
                }

                <Image source={timprofilepic} style={styles.profilepic}></Image>
                <Text style={styles.playertext}>{namaTim}</Text>
                <Text style={styles.pointText}>{pointim}</Text>
                {
                    rankTim == 1 ? <GoldMedalSVG style={styles.medal} /> : null
                }
                {
                    rankTim == 2 ? <SilverMedalSVG style={styles.medal} /> : null
                }
                {
                    rankTim == 3 ? <BronzeMedalSVG style={styles.medal} /> : null
                }
            </View>
            <View style={styles.line}></View>
            <SafeAreaView style={styles.scroll}>
                <FlatList
                    // style={styles.scroll}
                    horizontal={false}
                    data={dataPlayers}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => (
                        <LeaderboardPlayerCardViewMini
                            rank={0}
                            playername={item.playername}
                            poin={item.poin}
                            correctanswer={item.correctanswer}
                            wronganswer={item.wronganswer}
                            profilepic_uri={item.profilepic}
                            borderstatus={item.borderstatus}
                        />
                    )}
                />
            </SafeAreaView>
        </SafeAreaView>
    );
};

export default LeaderboardTeamCardView;

const styles = StyleSheet.create({
    line: {
        width: "100%",
        backgroundColor: 'white',
        height: 1,
    },
    container: {
        paddingHorizontal: 20,
        // flex:1,
        backgroundColor: '#00000099',
        // backgroundColor:'gray',
        marginHorizontal: 20,
        // marginBottom: 20,
        // paddingBottom:10,
        borderRadius: 20,
        // maxHeight:200,
        height:300,
        // paddingVertical:10,

    },
    scroll: {
        maxHeight: 200,
        // height:300,
        // marginTop:-20,
        paddingTop:-30,
        // backgroundColor: 'purple',
    },
    containerBordered: {
        paddingHorizontal: 20,
        backgroundColor: '#00000099',
        marginHorizontal: 20,
        marginBottom: 10,
        borderRadius: 20,
        // paddingVertical:10,
        borderWidth: 2,
        borderStyle: "solid",
        borderColor: "#BC3BF2",

    },
    horizontalcontainer: {
        flexDirection: 'row',
        // flex : 1,
        // alignContent:'stretch',
        // alignItems: 'stretch',
        // flexShrink: 1,
        justifyContent: 'space-between',
        // backgroundColor : "green",

    },
    profilepic: {
        marginHorizontal: 10,
        borderRadius: 20,
        width: 48,
        maxHeight: 48,
        height:48,
        // backgroundColor: 'black',
    },
    scorebar: {
        flexDirection: 'row',
        // backgroundColor: 'white',
        width: '100%',
        height: 7,
        marginTop: 10,
        borderRadius: 10,
    },
    medal: {
        marginTop: -10,
        // marginRight : -40,
    },
    textStyle: {
        // alignSelf: "center",
        color: "white",
        fontFamily: "avenir-next-reguler",
        fontWeight: "bold",
        fontSize: 14,
        verticalAlign: 'middle',
        // backgroundColor:'green',
    },
    pointText: {
        // alignSelf: "center",
        // flex :1,
        color: "white",
        fontFamily: "avenir-next-reguler",
        fontWeight: "bold",
        fontSize: 14,
        verticalAlign: 'middle',
        // marginRight : -20,
        // alignSelf: 'flex-start',
        // alignContent: 'flex-start',
        // flexDirection:'column'
        // backgroundColor:'green',
    },
    playertext: {
        // alignSelf: "center",
        color: "white",
        fontFamily: "avenir-next-reguler",
        fontWeight: "bold",
        fontSize: 14,
        verticalAlign: 'middle',
        // backgroundColor:'red',
        width: 150,
        paddingRight: 10,
        maxHeight: 48,
    },
});