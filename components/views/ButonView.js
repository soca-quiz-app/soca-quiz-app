import { StyleSheet, Text, TouchableHighlight } from 'react-native';
import React from 'react';

const ButonView = ({ text = '', onPress = null }) => {
  return (
    <TouchableHighlight style={styles.container} onPress={onPress}>
      <Text style={styles.title}>{text}</Text>
    </TouchableHighlight>
  );
};

export default ButonView;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#6D75F6',
    padding: 16,
    borderRadius: 10,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    marginHorizontal: 38,
  },
  title: {
    fontFamily: 'avenir-next-demibold',
    color: '#fff',
    fontWeight: '500',
    fontSize: 14,
  },
});
