import { StyleSheet, Text, View, Modal, TouchableOpacity ,Image} from "react-native";
import React, {useState} from "react";
import { BlurView } from "expo-blur";
import ContinueGoogleButton from "./ContinueGoogleButton";
import GoldMedalSVG from '../../assets/image/icon/GoldMedal.svg';
import SilverMedalSVG from '../../assets/image/icon/SilverMedal.svg';
import BronzeMedalSVG from '../../assets/image/icon/BronzeMedal.svg';

const LeaderboardPlayerCardView = ({ 
    profilepic_uri = require('../../assets/image/avatar/Male-Scientist.png'),
    playername = "Kevin", 
    borderstatus = false,
    poin = 120, 
    correctanswer =  6, 
    wronganswer =  4,
    rank = 1,
    opacity = 1,
     }) => {
        correct_bar_size = correctanswer / (correctanswer+wronganswer);
        corr_bar_size = (correct_bar_size*100) + "%";
        wrong_bar_size = (100 - (correct_bar_size*100)) + "%";

        styleadd = ""
        styleaddwrong = ""
        if (corr_bar_size == "100%") {
            styleadd = {
                borderTopRightRadius:10,
                borderBottomRightRadius:10,
            }
            styleaddwrong = ""
        }else if(corr_bar_size == "0%"){
            styleaddwrong = {
                borderTopLeftRadius:10,
                borderBottomLeftRadius:10,
            }
            styleadd = ""
        }

        
        

  return (
    <View style={borderstatus == true ? styles.containerBordered : styles.container}>
        <View style={styles.horizontalcontainer}>
            {
                rank > 0 ? <Text style={styles.textStyle}>#{rank}</Text> : null
            }
            
            <Image source = {profilepic_uri} style={styles.profilepic}></Image>
            <Text style={styles.playertext}>{playername}</Text>
            <Text style={styles.pointText}>{poin}</Text>
            {
                rank == 1 ? <GoldMedalSVG style={styles.medal}/> : null
            }
            {
                rank == 2 ? <SilverMedalSVG style={styles.medal}/> : null
            }
            {
                rank == 3 ? <BronzeMedalSVG style={styles.medal}/> : null
            }
        </View>
        <View style={styles.scorebar}>
            <View
            style={{
                backgroundColor:"#40BE45",
                width: corr_bar_size,
                height:'100%',
                borderTopLeftRadius:10,
                borderBottomLeftRadius:10,
                ...styleadd,
                
            }}/>
            <View 
            style={{
                backgroundColor:"#EB5757",
                width: wrong_bar_size,
                height:'100%',
                borderTopRightRadius:10,
                borderBottomRightRadius:10,
                ...styleaddwrong
            }}>
            </View>
        </View>
        <View style={styles.horizontalcontainer}>
        <Text style={styles.correctText}>Benar {correctanswer}</Text>
        <Text style={styles.wrongText}>Salah {wronganswer}</Text>
        </View>
        {/* <Text></Text> */}
        
        
    </View>
  );
};

export default LeaderboardPlayerCardView;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal:20,
    // backgroundColor:'#00000099',
    // marginHorizontal:20,
    // marginBottom:10,
    marginTop:5,
    // borderRadius:20,
    // paddingVertical:10,

  },
  containerBordered: {
    paddingHorizontal:20,
    // backgroundColor:'#00000099',
    // marginHorizontal:20,
    // marginBottom:10,
    // borderRadius:20,
    // paddingVertical:10,
    marginTop:5,
    borderWidth:1,
    borderStyle:"solid",
    borderColor:"#BC3BF2",

  },
  correctText:{
    flex:1,
    fontFamily: 'avenir-next-reguler',
    fontSize: 12,
    color: '#40BE45',
  },
  wrongText:{
    // flex:1,
    fontFamily: 'avenir-next-reguler',
    fontSize: 12,
    color: '#EB5757',
  },
  horizontalcontainer: {
    flexDirection:'row',
    
    // flex : 1,
    // alignContent:'stretch',
    // alignItems: 'stretch',
    // flexShrink: 1,
    justifyContent: 'space-between',
    // backgroundColor : "green",
    
  },
  profilepic:{
    marginHorizontal:10,
    borderRadius:50,
    width:30,
    maxHeight:30,
    backgroundColor : 'black',
  },
  scorebar:{
    flexDirection:'row',
    backgroundColor:'white',
    width:'100%',
    height:5,
    marginTop:10,
    borderRadius:10,
  },
  medal:{
    marginTop:-10,
    // marginRight : -40,
  },
  bodyContainer: {
    height: 235,
    width: 414,
    backgroundColor: "#1F1F1F",
    // backgroundColor: "green",
    alignContent: "center",
    justifyContent: "center",
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
  },
  textStyle: {
    // alignSelf: "center",
    color: "white",
    fontFamily: "avenir-next-reguler",
    fontWeight:"bold",
    fontSize: 12,
    verticalAlign:'middle',
    // backgroundColor:'green',
  },
  pointText: {
    // alignSelf: "center",
    // flex :1,
    color: "white",
    fontFamily: "avenir-next-reguler",
    fontWeight:"bold",
    fontSize: 12,
    verticalAlign:'middle',
    // marginRight : -20,
    // alignSelf: 'flex-start',
    // alignContent: 'flex-start',
    // flexDirection:'column'
    // backgroundColor:'green',
  },
  playertext: {
    // alignSelf: "center",
    color: "white",
    fontFamily: "avenir-next-reguler",
    fontWeight:"bold",
    fontSize: 12,
    verticalAlign:'middle',
    // backgroundColor:'red',
    width:150,
    paddingRight:10,
    maxHeight:48,
  },
  googleButtonStyle: {
    backgroundColor: "white",
    alignContent: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  buttonText: {
    fontFamily: "avenir-next-reguler",
    fontSize: 15,
    borderRadius: 25,
  },
  skipTextStyle: {
    alignSelf: "center",
    fontFamily: "avenir-next-reguler",
    fontSize: 17,
    color: "#8C8C8C",
    marginTop: 21,
    marginBottom: 33,
  },
});
