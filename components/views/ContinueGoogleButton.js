import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import GoogleLogo from '../../assets/Google.svg';

const ContinueGoogleButton = ({ text = '', borderRadius = 0, onPress = null, statate = null }) => {
  return (
    <TouchableOpacity style={[styles.container, { borderRadius: borderRadius }]} disabled={statate} onPress={onPress}>
      <GoogleLogo />
      <Text style={styles.title}>{text}</Text>
    </TouchableOpacity>
  );
};

export default ContinueGoogleButton;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 14,
    // borderRadius: 10,
    marginTop: 13,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    marginHorizontal: 38,
  },
  title: {
    fontFamily: 'avenir-next-demibold',
    color: '#000',
    fontWeight: '500',
    fontSize: 14,
    marginStart: 2,
  },
});
