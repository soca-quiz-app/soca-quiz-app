// import { StatusBar } from 'expo-status-bar';
// import { StyleSheet, Text, View } from 'react-native';
import * as Font from "expo-font";
import React, { useState } from "react";
import AppLoading from "expo-app-loading";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import AuthNavigation from "./components/routes/AuthNavigation";

const Stack = createStackNavigator();

// font settings
const getFont = () => {
  return Font.loadAsync({
    "avenir-next-reguler": require("./assets/fonts/AvenirNextLTPro-Regular.otf"),
    "avenir-next-demibold": require("./assets/fonts/AvenirNext-DemiBold.ttf"),
    "avenir-next-medium": require("./assets/fonts/AvenirNextCyr-Medium.ttf"),
    "avenir-next-it": require("./assets/fonts/AvenirNextLTPro-It.otf"),
    "avenir-next-bold": require("./assets/fonts/AvenirNextLTPro-Bold.otf"),
  });
};

export default function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);
  if (fontsLoaded) {
    return (
      <NavigationContainer>
        {/* <BottomNavigation /> */}
        {/* <PlayNavigation /> */}
        {/* <LeaderboardNavigation></LeaderboardNavigation> */}
        <AuthNavigation />
      </NavigationContainer>
    );
  } else {
    return (
      <AppLoading
        startAsync={getFont}
        onFinish={() => setFontsLoaded(true)}
        onError={() => console.log("error")}
      />
    );
  }
}
