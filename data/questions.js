
export default [
   
  {
      jenis_pertanyaan: "ganda",
      isCode : true,
      code : "file = open('file.txt', 'r')\nprint(file.read())",
      src: require('../assets/soekarno.png'),
      question: "Apa yang dilakukan Ir. Soekarno pada gambar ?",
      options:[
          {
              id:"0",
              options:"A",
              answer:"South America",
          },
          {
              id:"1",
              options:"B",
              answer:"Europe",
          },
          {
              id:"2",
              options:"C",
              answer:"Asia",
          },
          {
              id:"0",
              options:"D",
              answer:"India",
          },
      ],
      correctAnswerIndex: 2
  },
  {
      jenis_pertanyaan: "ganda",
      question: "data",
      options:[
          {
              id:"0",
              options:"A",
              answer:"Asia",
          },
          {
              id:"1",
              options:"B",
              answer:"South Africa",
          },
          {
              id:"2",
              options:"C",
              answer:"Australia",
          },
          {
              id:"0",
              options:"D",
              answer:"Antarctica",
          },
      ],
      correctAnswerIndex: 0,

  },
  {
      jenis_pertanyaan: "audio",
      question: "",
      src : require('../assets/music/sounds1.mp3'),
      options:[
          {
              id:"0",
              options:"A",
              answer:"20",
          },
          {
              id:"1",
              options:"B",
              answer:"25",
          },
          {
              id:"2",
              options:"C",
              answer:"10",
          },
          {
              id:"0",
              options:"D",
              answer:"30",
          },
      ],
      correctAnswerIndex: 1
  },
  {
      jenis_pertanyaan: "audio",
      question: "what is the square root of 169",
      options:[
          {
              id:"0",
              options:"A",
              answer:"20",
          },
          {
              id:"1",
              options:"B",
              answer:"23",
          },
          {
              id:"2",
              options:"C",
              answer:"13",
          },
          {
              id:"0",
              options:"D",
              answer:"23",
          },
      ],
      correctAnswerIndex: 2
  },
  {
      question: "What is the Smallest Ocean?",
      options:[
          {
              id:"0",
              options:"A",
              answer:"Atlantic Ocean",
          },
          {
              id:"1",
              options:"B",
              answer:"Pacific Ocean",
          },
          {
              id:"2",
              options:"C",
              answer:"Arctic Ocean",
          },
          {
              id:"0",
              options:"D",
              answer:"Indian Ocean",
          },
      ],
      correctAnswerIndex: 2
  }
]